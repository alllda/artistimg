import socket
import urllib
import httplib
import sys
from urlparse import urlparse
import os
import threading
import Queue
import socket
socket.setdefaulttimeout(3)

def downloadUrls(urlList, numConcurrent, skipExisting, dir):
    # prepare the queue
    queue = Queue.Queue()
    for url in urlList:
        queue.put(url)

    # start the requested number of download threads to download the files
    threads = []
    for _ in range(numConcurrent):
        t = DownloadThread(queue, skipExisting, dir)
        t.daemon = True
        t.start()

    queue.join()

class DownloadThread(threading.Thread):
    def __init__(self, queue, skipExisting, dir):
        super(DownloadThread, self).__init__()
        self.queue = queue
        self.skipExisting = skipExisting
        self.dir = dir

    def run(self):
        while True:
            #grabs url from queue
            url = self.queue.get()
            path = getPath(url, self.dir)

            if self.skipExisting and os.path.exists(path):
                # skip if requested
                #print "Skip '%s'"%path
                self.queue.task_done()
                continue

            try:
                urllib.urlretrieve(url, path)
		#print "Downloaded: '%s'" % url
            except:
                #print "Error downloading url '%s'." % url
		pass
            #signals to queue job is done
            self.queue.task_done()

def getPath(url, dir):
    name = url.split('/')[-1]
    path = str(dir) + "/" + str(name)
    return path

def main():
    if len(sys.argv) == 2 and (sys.argv[1] == '-h' or sys.argv[1] == '--help'):
        printHelp()
        sys.exit()
    if len(sys.argv) != 3:
        print 'Bad arguments'
        printHelp()
        sys.exit()
    try:
        file = open(sys.argv[1],'r')
    except:
        print 'Can\'t open file'
        sys.exit()
    if not os.path.isdir(sys.argv[2]):
        print 'Second argument is not directory'
        sys.exit()
    urlList = []
    counter = 0
    for line in file:
        parsedLine = line.split('\t')
        if len(parsedLine) == 3:
            url = parsedLine[-1].strip()
            urlList.append(url)
    downloadUrls(urlList,60,True,sys.argv[2])



if __name__ == "__main__":
        main()
