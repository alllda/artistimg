# File name: imageList.py
# Autor: Ales Raszka

import gzip
import io
import os
from BeautifulSoup import BeautifulSoup
from urlparse import urljoin
import sys
def findImg(file,nameDict,fileout):
    warcpart = file.split('WARC/1.0')
    length =  len(warcpart)
    count = 1
    outputString = ""
    #globalList = []
    for item in warcpart[2:]:   #iterate all html pages
	print (str(count) + '/' + str(length-2))
	uri = item.split('WARC-Target-URI: ')[1].split('\n')[0].decode('utf-8')
	count += 1
	haveImages = False
	imageFirst = []
	for key in nameDict.keys():	
	    #print 'hledam ' + nameDict[key]
	    if nameDict[key] in item and nameDict[key] != '':	#find name at page
		#print 'nalezen ' + nameDict[key]
		if not haveImages:
		    haveImages = True
		    try:
			soup = BeautifulSoup(item)  #html parse
			#print "Nasel jsem jmeno:" + name
			images = soup.findAll('img') #find images
		    except:
			continue
		    for img in images:
			try:
			    alt = img.get('alt','')
			    src = img.get('src','')
			    url = urljoin(uri,src)
			    #localList = [key,nameDict[key],uri,url,alt]
			    #if localList not in globalList:
				#globalList.append(localList)
			    imageFirst.append(uri+"\t"+url+"\t"+alt+"\n")
			    fileout.write(u'{}\t{}\t{}\t{}\t{}\n'.format(key,nameDict[key].decode('utf-8'),uri,url,alt))
			except:
			    continue
		else:
		    for val in imageFirst:
			#print type(val)
			#outputString += (str(key)+";"+nameDict[key]+";"+val)
			#outputString += key
			#outputString += ";"
			#outputString += nameDict[key]
			#outputString += ";"
			#outputString += str(val)
			#outputString += "%s;%s;%s" % (key,nameDict[key],val)
			fileout.write(u'{}\t{}\t{}'.format(key,nameDict[key].decode('utf-8'),val))
			



def getArtistDict(file):
    file = open(file,'r')

    artist = {}
    try:
	for line in file:
	    artist[line.split(';')[0]] = line.split(';')[1].replace('\n','')
	file.close()
    except:
	print 'Artist file is not in correct format'
	sys.exit()
    return artist

def printHelp():
    print '-----------------------------------------------------------------------------------'
    print 'Script search for artist in .warc files and save info about all images on HTML page'
    print 'Output format:'
    print '    ID;name;image;webpage URL;image URL;alt'
    print 'Usage:'
    print '    python imageList.py --help or -h'
    print '    python imageList.py arg1 artistFile output'
    print '        arg1 - .warc file or path where look for .warc file'
    print '        artistFile - file in CSV format'
    print '                   - ID;Name '
    print '        output - output file '
    print '-----------------------------------------------------------------------------------'

def main():
    if(len(sys.argv) == 2 and (sys.argv[1] == '--help' or sys.argv[1] == '-h')):
	printHelp()
	sys.exit()
    elif(len(sys.argv) == 4):
	#get artist dictionary from file in argv[2]
	artists = getArtistDict(sys.argv[2])
	#open output file
	try:
	    fileOut = io.open(sys.argv[3],'w',encoding='utf-8')
	except:
	    print 'Can\'t open output file'
	    sys.exit()
	if(not os.path.exists(sys.argv[1])):
	    print 'Path not exists'
	    sys.exit()
	#argument option -f = file
	if(os.path.isfile(sys.argv[1])):
	    file = gzip.open(sys.argv[1],'r').read()
	    print 'Searching in:' + str(sys.argv[1])
	    findImg(file,artists,fileOut)
	#argument option -p = path
	elif(os.path.isdir(sys.argv[1])):
	    #browse through directories 
	    for dirname, dirnames, filenames in os.walk(sys.argv[1]):
		for filename in filenames:
		    filePath = os.path.join(dirname, filename)
		    #only use .warc files
		    if filePath.endswith('.warc.gz'):
			file = gzip.open(filePath,'r').read()
			print 'Searching in:' + str(filePath)
			findImg(file,artists,fileOut)
	else:
	    print 'Bad arguments'
	    printHelp()
	fileOut.close()
	sys.exit()
    else:
	print 'Bad arguments'
	printHelp()
	sys.exit()

if __name__ == "__main__":
        main()
