import gzip
import io
import sys
import os
from BeautifulSoup import BeautifulSoup
from urlparse import urljoin
from multiprocessing import Process, Manager

def printHelp():
    print('-----------------------------------------------------------------------------------')
    print('Script search for artist in .warc files and save info about all images on HTML page')
    print('Output format:')
    print('    ID;name;image;webpage URL;image URL;alt')
    print('Usage:')
    print('    python imageList.py --help or -h')
    print('    python imageList.py arg1 artistFile output')
    print('        arg1 - .warc file or path where look for .warc file')
    print('        artistFile - file in CSV format')
    print('                   - ID;Name ')
    print('        output - output file ')
    print('-----------------------------------------------------------------------------------')



def findInPage(page, artistsDict, output,id):
	foundImages = False
	imagesFirst = []
	for key in artistsDict.keys():
		if artistsDict[key] in page:
			#print(artistsDict[key])
			#print(str(id)+ " " +artistsDict[key])
			if not foundImages:
				foundImages = True
				try:
					#print "pred soup" + str(id)
					soup = BeautifulSoup(page)
					#print "pred IMG" + str(id)
					images = soup.findAll('img')
					#print "parse soup" + str(id)
				except:
					print "Chyba"
					return
				#print "celkem fotek: " +str(len(images))
				if not images:
					#print "koncim hned"
					return
				uri = page.split('WARC-Target-URI: ')[1].split('\n')[0].decode('utf-8').strip()
				#print "URI: " +uri
				for img in images:
					try:
						#print img
						alt = img.get('alt','')
						#print "ALT: "+ alt
						src = img.get('src','')
						#print "SRC: " + src
						url = urljoin(uri,src)
						#print "URL: "+url
						imagesFirst.append(uri+"\t"+url+"\t"+alt+"\n")
						#print('{}\t{}\t{}\t{}\t{}\n'.format(key,artistsDict[key].decode('utf-8'),uri,url,alt))
						output.write(u'{}\t{}\t{}\t{}\t{}\n'.format(key,artistsDict[key].decode('utf-8'),uri,url,alt))
					except:
						print "Chyba konec"
						return
			else:
				for val in imagesFirst:
					#print artistsDict[key]
					#print(len(imagesFirst))
					#print(u'{}\t{}\t{}'.format(key,artistsDict[key].decode('utf-8'),val))
					output.write(u'{}\t{}\t{}'.format(key,artistsDict[key].decode('utf-8'),val))
	print(str(id)+" Done")
	return

def findImages(input, output, artistsDict):
	inputParts = input.split('WARC/1.0')
	pageCount = len(inputParts)
	print("Total pages: "+str(len(inputParts)))
	processList = []
	counter = 0
	counter2 = 0
	for page in inputParts[2:]: #13264
		#print page
		counter2 +=1
		if(counter < 30):
			p = Process(target=findInPage, args=(page,artistsDict,output,counter2))
			processList.append(p)
			p.start()
			counter += 1
		else:
			for p in processList:
				#print("cekam na " +str(counter2))
				p.join()
				#print("skoncil " +str(counter2))
			processList = []
			counter = 1
			p = Process(target=findInPage, args=(page,artistsDict,output,counter2))
			processList.append(p)
			p.start()
	if processList:
		for p in processList:
			p.join()


def getArtistDict(file):
	file = open(file,'r')

	artist = {}
	try:
		for line in file:
			artist[line.split("\t")[0][2:]] = line.split("\t")[2].replace('\n','')
			#print line.split("\t")[0][2:] + " -- " + line.split("\t")[2].replace('\n','')
		file.close()
	except:
		print ('Artist file is not in correct format')
		sys.exit()
	return artist

def main():
	if(len(sys.argv) == 2 and (sys.argv[1] == '--help' or sys.argv[1] == '-h')):
		printHelp()
		sys.exit()
	elif(len(sys.argv) == 4):
		#get artist dictionary from file in argv[2]
		artists = getArtistDict(sys.argv[2])
		#open output file
		try:
		    fileOut = io.open(sys.argv[3],'w',encoding='utf-8')
		except:
		    print('Can\'t open output file')
		    sys.exit()
		if(not os.path.exists(sys.argv[1])):
		    print('Path not exists')
		    sys.exit()
		#argument option -f = file
		if(os.path.isfile(sys.argv[1])):
		    file = gzip.open(sys.argv[1],'r').read()
		    print('Searching in:' + str(sys.argv[1]))
		    findImages(file,fileOut,artists)
		#argument option -p = path
		elif(os.path.isdir(sys.argv[1])):
		    #browse through directories 
			for dirname, dirnames, filenames in os.walk(sys.argv[1]):
				for filename in filenames:
					filePath = os.path.join(dirname, filename)
					 #only use .warc files
					if filePath.endswith('.warc.gz'):
						file = gzip.open(filePath,'r').read()
						print('Searching in:' + str(filePath))
						findImages(file,fileOut,artists)
		else:
		    print ('Bad arguments')
		    printHelp()
		fileOut.close()
		sys.exit()
	else:
		print ('Bad arguments')
		printHelp()
		sys.exit()

if __name__ == "__main__":
        main()
			
