/* File: str2bin.cc
 * Date: 3/2014
 * Author: Peter Hostacny, xhosta03@stud.fit.vutbr.cz
 * Project: Decipher_fsa (function make_automat)
 * Description: Program is converting line numbers from string format do binary. 
 */
       
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>

using namespace std;

unsigned int number_load(int *loaded_char);
void number_print(unsigned int number);

int main() 
{
    /* Program nacitava znaky z stdin a hlada prvy znak '|' v riadku.
     * Po jeho najdeni nacita cele cislo nachadzajuce sa bezprostredne za nim
     * a prevedie ho na binarnu podobu (zakodovane) vypise ho na stdout a nacita dalsi znak.
     * Ak je dalsi znak ';' vypise ho na stdout, nacita dalsie cislo, prevedie a vypise ho.
     * Ak narazi na '\n' vypise ho na stdout a pokracuje dalsim riadkom.
     * Vsetky znaky (aj oddelovacie) okrem cisel riadkov su bez zmeny prepisovane na stdout.
     */
	int loaded_char = 0;   // prave nacitany znak
	unsigned int number = 0;  // cislo pre prevod do binarnej podoby
	do
	{
		loaded_char = getchar();   // nacitanie znaku zo vstupu
		if (loaded_char == '|')	   // najdenie prveho '|' v riadku 
		{
		    putchar(loaded_char);
		    do   // nacitanie cisla a konvertovanie do binarnej podoby
		    {
			number = number_load(&loaded_char);  // nacitanie cisla zo vstupu
			if (loaded_char == 'N')	// tento riadok patri fragmentu
			{
			    putchar(loaded_char);   // vytlacenie 'N'
			    loaded_char = getchar();   // nacitanie dalsieho znaku ('\n')
			}
			else
			    number_print(number);  // vypis cisla na vystup v binarnej podobe (zaberie 4 byty)
			if (loaded_char != ';')
			    putchar(loaded_char);
		    } while (loaded_char == ';');
		}
		else
		{
		    putchar(loaded_char);
		}

	} while (loaded_char != EOF);

	return EXIT_SUCCESS;
}   // koniec main()


/* Nacitanie cislic ako znakov a prevod na int */
unsigned int number_load (int *loaded_char)
{
	string str;
	int i = 0;
	do {  // nacita lubovolne dlhu postupnost cislic
		*loaded_char = getchar();  // nacitanie cislic po jednotlivych znakoch
		str[i] = (char)*loaded_char;	// kopirovanie znakov do retazca
		i++;	// inkrementacia pre indexovanie znakov v retazci
	} while  ((*loaded_char >= '0') && (*loaded_char <= '9'));
	
	return atoi(str.c_str());  // vrati cislo ziskane zo stringu
}


/* Vytlacenie cisla v binarnej podobe na vystup */
void number_print(unsigned int number)
{
/* Funkcia vytlaci cislo v binarnej podobe na stdout.
 * - cislo zaberie 4 byty
 * - z cisla bude vytlacenych len 28 spodnych bitov z celkoveho poctu 32 (max. mozne cislo 268435456)
 * - cislo je zakodovane v podobe 4x( [7 bitov cisla]+[1 bit s hodnotou 1] )
 */
	int temp_number = 0; // docase cislo pre posuvanie bitov

	for (int i = 21; i >= 0; i-=7)	// posuv po 7 bitoch (dokopy sa ulozi 28 bitov)
	{
		temp_number = number >> i; // bitovy posuv o nasobky 7
		temp_number <<= 1;	// posunutie bitov kvoli zmene LSB na 1
		temp_number |= 0x01;	// LSB je nastaveny na 1 v kazdom zapisanom bajte
		putchar(temp_number & 0xFF);  // odfiltrovanie vsetkych bitov okrem spodnych 8
	}
}

