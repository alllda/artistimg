#!/bin/sh
export LC_ALL="C"
# posledni uprava: 23.4.2014

# $1 = KB.all (knowledge base)
#======================================================================
# vytvoreni seznamu klicu entit v KB
python KB2namelist_autocomplete.py < "$1" | tr -s ' ' > intext_auto
#======================================================================
cat intext_auto | grep "^p:" | sed 's/^p:\t//' > p_intext
cat intext_auto | grep "^a:" | sed 's/^a:\t//' > a_intext
cat intext_auto | grep "^l:" | sed 's/^l:\t//' > l_intext
cat intext_auto | grep "^w:" | sed 's/^w:\t//' > w_intext
cat intext_auto | grep "^c:" | sed 's/^c:\t//' > c_intext
cat intext_auto | grep "^e:" | sed 's/^e:\t//' > e_intext
cat intext_auto | grep "^f:" | sed 's/^f:\t//' > f_intext
cat intext_auto | grep "^d:" | sed 's/^d:\t//' > d_intext
cat intext_auto | grep "^m:" | sed 's/^m:\t//' > m_intext
cat intext_auto | grep "^g:" | sed 's/^g:\t//' > g_intext
cat intext_auto | grep "^n:" | sed 's/^n:\t//' > n_intext
cat intext_auto | grep "^y:" | sed 's/^y:\t//' > y_intext
cat intext_auto | grep "^i:" | sed 's/^i:\t//' > i_intext
cat intext_auto | grep "^r:" | sed 's/^r:\t//' > r_intext
cut -f2- intext_auto > x_intext
#======================================================================
# skript, ktery slouci duplicty (cisla radku do jednoho)
python uniq_namelist.py < p_intext | tr '\t' '|' > p_namelist.UTF-8
python uniq_namelist.py < a_intext | tr '\t' '|' > a_namelist.UTF-8
python uniq_namelist.py < l_intext | tr '\t' '|' > l_namelist.UTF-8
python uniq_namelist.py < w_intext | tr '\t' '|' > w_namelist.UTF-8
python uniq_namelist.py < c_intext | tr '\t' '|' > c_namelist.UTF-8
python uniq_namelist.py < e_intext | tr '\t' '|' > e_namelist.UTF-8
python uniq_namelist.py < f_intext | tr '\t' '|' > f_namelist.UTF-8
python uniq_namelist.py < d_intext | tr '\t' '|' > d_namelist.UTF-8
python uniq_namelist.py < m_intext | tr '\t' '|' > m_namelist.UTF-8
python uniq_namelist.py < g_intext | tr '\t' '|' > g_namelist.UTF-8
python uniq_namelist.py < n_intext | tr '\t' '|' > n_namelist.UTF-8
python uniq_namelist.py < x_intext | tr '\t' '|' > x_namelist.UTF-8
python uniq_namelist.py < y_intext | tr '\t' '|' > y_namelist.UTF-8
python uniq_namelist.py < i_intext | tr '\t' '|' > i_namelist.UTF-8
python uniq_namelist.py < r_intext | tr '\t' '|' > r_namelist.UTF-8
#======================================================================
./utf2symbols < p_namelist.UTF-8 | sort -u > p_namelist.ASCII
./utf2symbols < a_namelist.UTF-8 | sort -u > a_namelist.ASCII
./utf2symbols < l_namelist.UTF-8 | sort -u > l_namelist.ASCII
./utf2symbols < w_namelist.UTF-8 | sort -u > w_namelist.ASCII
./utf2symbols < c_namelist.UTF-8 | sort -u > c_namelist.ASCII
./utf2symbols < e_namelist.UTF-8 | sort -u > e_namelist.ASCII
./utf2symbols < f_namelist.UTF-8 | sort -u > f_namelist.ASCII
./utf2symbols < d_namelist.UTF-8 | sort -u > d_namelist.ASCII
./utf2symbols < m_namelist.UTF-8 | sort -u > m_namelist.ASCII
./utf2symbols < g_namelist.UTF-8 | sort -u > g_namelist.ASCII
./utf2symbols < n_namelist.UTF-8 | sort -u > n_namelist.ASCII
./utf2symbols < x_namelist.UTF-8 | sort -u > x_namelist.ASCII
./utf2symbols < y_namelist.UTF-8 | sort -u > y_namelist.ASCII
./utf2symbols < i_namelist.UTF-8 | sort -u > i_namelist.ASCII
./utf2symbols < r_namelist.UTF-8 | sort -u > r_namelist.ASCII
#=====================================================================
# prevod cisel z formatu string do binarnej podoby
./str2bin < p_namelist.ASCII > p_namelist.ASCII.bin
./str2bin < a_namelist.ASCII > a_namelist.ASCII.bin
./str2bin < l_namelist.ASCII > l_namelist.ASCII.bin
./str2bin < w_namelist.ASCII > w_namelist.ASCII.bin
./str2bin < c_namelist.ASCII > c_namelist.ASCII.bin
./str2bin < e_namelist.ASCII > e_namelist.ASCII.bin
./str2bin < f_namelist.ASCII > f_namelist.ASCII.bin
./str2bin < d_namelist.ASCII > d_namelist.ASCII.bin
./str2bin < m_namelist.ASCII > m_namelist.ASCII.bin
./str2bin < g_namelist.ASCII > g_namelist.ASCII.bin
./str2bin < n_namelist.ASCII > n_namelist.ASCII.bin
./str2bin < x_namelist.ASCII > x_namelist.ASCII.bin
./str2bin < y_namelist.ASCII > y_namelist.ASCII.bin
./str2bin < i_namelist.ASCII > i_namelist.ASCII.bin
./str2bin < r_namelist.ASCII > r_namelist.ASCII.bin
#======================================================================
# vytvoreni konecneho automatu
fsa/fsa_build < p_namelist.ASCII.bin > ../p_automata.fsa
fsa/fsa_build < a_namelist.ASCII.bin > ../a_automata.fsa
fsa/fsa_build < l_namelist.ASCII.bin > ../l_automata.fsa
fsa/fsa_build < w_namelist.ASCII.bin > ../w_automata.fsa
fsa/fsa_build < c_namelist.ASCII.bin > ../c_automata.fsa
fsa/fsa_build < e_namelist.ASCII.bin > ../e_automata.fsa
fsa/fsa_build < f_namelist.ASCII.bin > ../f_automata.fsa
fsa/fsa_build < d_namelist.ASCII.bin > ../d_automata.fsa
fsa/fsa_build < m_namelist.ASCII.bin > ../m_automata.fsa
fsa/fsa_build < g_namelist.ASCII.bin > ../g_automata.fsa
fsa/fsa_build < n_namelist.ASCII.bin > ../n_automata.fsa
fsa/fsa_build < x_namelist.ASCII.bin > ../x_automata.fsa
fsa/fsa_build < y_namelist.ASCII.bin > ../y_automata.fsa
fsa/fsa_build < i_namelist.ASCII.bin > ../i_automata.fsa
fsa/fsa_build < r_namelist.ASCII.bin > ../r_automata.fsa
#=====================================================================
rm intext_auto *_intext
rm *_namelist.UTF-8
rm *_namelist.ASCII
rm *_namelist.ASCII.bin
