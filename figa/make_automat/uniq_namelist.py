#!/usr/bin/env python

import sys

dictionary = {}

if __name__ == "__main__":
    for line in sys.stdin:
        split_line = line[:-1].split("\t")
        if split_line[0] not in dictionary:
            dictionary[split_line[0]] = set()
        dictionary[split_line[0]] |= set(split_line[1].split(";"))

    for k in sorted(dictionary.keys()):
        ids = sorted(dictionary[k])
        print k+ "\t" + ";".join(ids)
        
