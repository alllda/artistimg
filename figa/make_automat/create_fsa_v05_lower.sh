#!/bin/sh
export LC_ALL="C"
# posledni uprava: 9.7.2014

# $1 = KB.all (knowledge base) & $2 = names (fragmenty jmen a prijmeni) $3 = activities (slovesa)
#=====================================================================
# uprava fragmentu jmen a prijmeni entit
cat "$2" | sort -u | sed 's/$/\tN/' | grep -v "^'" | grep -v "^-" > fragments
# uprava sloves (kapitalizace)
cut -d'	' -f1 < "$3" | sed 's/$/\tN/' > verbs
cut -d'	' -f1 < "$3" | sed 's/./\u&/' | sed 's/$/\tN/' >> verbs
#=====================================================================
# vytvoreni seznamu klicu entit v KB, pridani fragmentu a sloves
python KB2namelist.py -l < "$1" | tr -s ' ' > intext
cat fragments verbs >> intext
python uniq_namelist.py < intext > namelist
#=====================================================================
# uprava stoplistu (kapitalizace a razeni)
cp stop_list stop_list.all
cat stop_list | sed -e 's/\b\(.\)/\u\1/g' >> stop_list.all
sort -u stop_list.all > stop_list.all.sorted
# odstranovani slov ze stoplistu
join -1 1 -2 1 -t'	' stop_list.all.sorted namelist > remove
comm -23 namelist remove | tr '\t' '|' > namelist.UTF-8
#=====================================================================
# prevod z UTF-8 znaku na ASCII reprezentaci
./utf2symbols < namelist.UTF-8 > namelist.ASCII
#=====================================================================
# prevod cisel z formatu string do binarnej podoby
./str2bin < namelist.ASCII > namelist.ASCII.bin
#=====================================================================
# vytvoreni konecneho automatu
echo "FSA BUILD automata-lower.fsa"
fsa/fsa_build < namelist.ASCII.bin > ../automata-lower.fsa
#=====================================================================
# smazani pomocnych souboru
rm fragments verbs
rm intext namelist
rm stop_list.all stop_list.all.sorted
rm remove
rm namelist.UTF-8 
rm namelist.ASCII
rm namelist.ASCII.bin
