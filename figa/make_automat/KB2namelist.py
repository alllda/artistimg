# -*- coding: utf-8 -*-

import argparse
import os
import unicodedata
import re
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

parser = argparse.ArgumentParser()
parser.add_argument("-l", "--lower", action="store_true", help="creates lowercase list")
args = parser.parse_args()

dictionary = {}

######################################################################
def remove_accent(input_str):
        nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
        return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

######################################################################
def add_to_dictionary(_key, _value, _type):
	_key = re.sub('\s+', ' ', _key).strip()
	if _key not in allow_list:
		unsuitable = ";?!()[]{}<>/~@#$%^&*_=+|\"\\"
		_key = _key.strip()
		for x in unsuitable:
			if x in _key:
				return

		if len(re.findall(r"[0-9]+", _key)) != 0:
			if len(re.findall(r"^[0-9 ]+$", _key)) != 0:
				return
			if _type in "pa":
				if len(re.findall(r"[0-9]+[stndrh]{2}", _key)) == 0:
					return
			if _type in "l":
				return
			if _type in "e":
				if len(re.findall(r"century", _key)) != 0:
					return
				if len(re.findall(r"centuries", _key)) != 0:
					return

		if _type in "pa":
			if "," in _key:
				return
			if _key.startswith("The "):
				return
			if _key.startswith("List of "):
				return

		if _type in "palwceny":
			if len(re.findall(r"^[a-z]", _key)) != 0:
				return

		if _type in "w":
			if _key in firstname_list:
				return

		if _key in nationality_list and _type not in "n":
			return

		if len(_key.decode("utf-8")) < 2 or len(_key.decode("utf-8")) > 80:
			return

		if _type not in "pa":
			if len(re.findall(r"[.,;]$", _key)) != 0:
				return

	add(_key, _value, _type)

	# adding alternatives

	if _type in "palwce":
		if "Saint " in _key:
			add(re.sub(r"Saint ", "St. ", _key), _value, _type) # Saint John -> St. John
			add(re.sub(r"Saint ", "St.", _key), _value, _type) # Saint John -> St.John
			add(re.sub(r"Saint ", "St ", _key), _value, _type) # Saint John -> St John
		if "St " in _key:
			add(re.sub(r"St ", "St. ", _key), _value, _type) # St John -> St. John
			add(re.sub(r"St ", "St.", _key), _value, _type) # St John -> St.John
			add(re.sub(r"St ", "Saint ", _key), _value, _type) # St John -> Saint John
		if "St." in _key:
			if "St. " in _key:
				add(re.sub(r"St\. ", "St ", _key), _value, _type) # St. John -> St John
				add(re.sub(r"St\. ", "St.", _key), _value, _type) # St. John -> St.John
				add(re.sub(r"St\. ", "Saint ", _key), _value, _type) # St. John -> Saint John
			else:
				add(re.sub(r"St\.", "St ", _key), _value, _type) # St.John -> St John
				add(re.sub(r"St\.", "St. ", _key), _value, _type) # St.John -> St. John
				add(re.sub(r"St\.", "Saint ", _key), _value, _type) # St.John -> Saint John

	if _type in "pa":
		add(re.sub(r"([A-Z])[a-z]* ([A-Z][a-z]*)", "\g<1>. \g<2>", _key), _value, _type) # Adolf Born -> A. Born
		add(re.sub(r"([A-Z])[a-z]* ([A-Z])[a-z]* ([A-Z][a-z]*)", "\g<1>. \g<2>. \g<3>", _key), _value, _type) # Peter Paul Rubens -> P. P. Rubens
		add(re.sub(r"([A-Z][a-z]*) ([A-Z])[a-z]* ([A-Z][a-z]*)", "\g<1> \g<2>. \g<3>", _key), _value, _type) # Peter Paul Rubens -> Peter P. Rubens
		if "Mc" in _key:
			add(re.sub(r"Mc([A-Z])", "Mc \g<1>", _key), _value, _type) # McCollum -> Mc Collum
			add(re.sub(r"Mc ([A-Z])", "Mc\g<1>", _key), _value, _type) # Mc Collum -> McCollum
		if "." in _key:
			new_key = re.sub(r"([A-Z])\. (?=[A-Z])", "\g<1>.", _key) # J. M. W. Turner -> J.M.W.Turner
			add(new_key, _value, _type)
			new_key = re.sub(r"([A-Z])\.(?=[A-Z][a-z]+)", "\g<1>. ", new_key) # J.M.W.Turner -> J.M.W. Turner
			add(new_key, _value, _type)
			add(re.sub(r"\.", "", new_key), _value, _type) # J.M.W. Turner -> JMW Turner
		if "-" in _key:
			add(re.sub(r"\-", " ", _key), _value, _type) # Payne-John Christo -> Payne John Christo
		if "ì" in _key:
			add(re.sub("ì", "í", _key), _value, _type) # Melozzo da Forlì -> Melozzo da Forlí

		for x in f_name:
			if x in _key:
				new_key = re.sub(' ?,? '+x+'$', '', _key) # John Brown, Jr. -> John Brown
				add(re.sub('^'+x+' ', '', new_key), _value, _type) # Sir Patrick Stewart -> Patrick Stewart
	if _type in "m":
		add(_key.lower(), _value, _type) # Baroque -> baroque
		if _key.endswith("ism"):
			add(re.sub("ism", "ist", _key), _value, _type) # Symbolism -> Symbolist
		if _key.endswith("ist"):
			add(re.sub("ist", "ism", _key), _value, _type) # Symbolist -> Symbolism

	if _type in "l":
		if "County" in _key:
			new_key = re.sub(r" County$", "", _key) # Londonderry County -> Londonderry
			add(re.sub(r"^County (?=[A-Z])", "", new_key), _value, _type) # County Londonderry -> Londonderry

######################################################################
def add(_key, _value, _type):
	_key = _key.strip()

	if args.lower:
		_key = _key.lower()

	if len(re.findall(r"^[ '-\.]", _key)) != 0:
		return

	if _key not in dictionary:
		dictionary[_key] = set()
	dictionary[_key].add(_value)

	# removing accent
	_accent = remove_accent(_key)

	if _accent not in dictionary:
		dictionary[_accent] = set()
	dictionary[_accent].add(_value)

######################################################################
def process_location(_l, _line_num):
	aliases = _l[3].split("|")
	aliases.append(_l[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "l")

######################################################################
def process_artwork(_w, _line_num):
	aliases = _w[3].split("|")
	aliases.append(_w[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "w")

######################################################################
def process_artist(_a, _line_num):
	aliases = _a[4].split("|")
	aliases.append(_a[3])
	aliases.append(_a[2])

	year = _a[10]
	preferred_term = _a[3]
	wikipedia = _a[23]
	surname = ""

	if len(re.findall(r"^[0-9][0-9]{3}", year)) != 0:
		year = int(year[:4])
	else:
		year = 9999

	for t in aliases:
		length = t.count(" ") + 1
		if length >= 2:
			add_to_dictionary(t, _line_num, "a")
		elif year < 1900 and wikipedia != "":
			add_to_dictionary(t, _line_num, "a")

	if year < 1900 and wikipedia != "":
		if "," in preferred_term:
			surname = preferred_term.split(",")[0].strip()
			add_to_dictionary(surname, _line_num, "a")

#####################################################################
def process_event(_e, _line_num):
	aliases = _e[3].split("|")
	aliases.append(_e[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "e")

#####################################################################
def process_art_form(_f, _line_num):
	aliases = _f[3].split("|")
	aliases.append(_f[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "f")

#####################################################################
def process_art_genre(_g, _line_num):
	aliases = _g[3].split("|")
	aliases.append(_g[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "g")

#####################################################################
def process_art_period_movement(_m, _line_num):
	aliases = _m[3].split("|")
	aliases.append(_m[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "m")

#####################################################################
def process_visual_art_medium(_d, _line_num):
	aliases = _d[3].split("|")
	aliases.append(_d[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "d")

#####################################################################
def process_nationality(_n, _line_num):
	aliases = _n[8].split("|")
	for t in aliases:
		add_to_dictionary(t, _line_num, "n")

#####################################################################
def process_museum(_c, _line_num):
	aliases = _c[3].split("|")
	aliases.append(_c[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "c")

#####################################################################
def process_mythology(_c, _line_num):
	aliases = _c[3].split("|")
	aliases.append(_c[2])
	for t in aliases:
                length = t.count(" ") + 1
                if length >= 2 or t == _c[2]:
                        add_to_dictionary(t, _line_num, "y")

#####################################################################
def process_family(_c, _line_num):
	aliases = _c[3].split("|")
	aliases.append(_c[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "i")

#####################################################################
def process_group(_c, _line_num):
	aliases = _c[3].split("|")
	aliases.append(_c[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "r")

#####################################################################
def process_person(_p, _line_num):
	aliases = _p[3].split("|")
	aliases.append(_p[2])
	for t in aliases:
		length = t.count(" ") + 1
		if length >= 2:
			add_to_dictionary(t, _line_num, "p")

#####################################################################
if __name__ == "__main__":
	f_file = open("freq_terms_filtred.all","r")
	f_name = f_file.read().splitlines()
	f_file.close()

	allow_file = open("allow_list", "r")
	allow_list = allow_file.read().splitlines()
	allow_file.close()

	firstname_file = open("yob2012.txt", "r")
	firstname_list = firstname_file.read().splitlines()
	firstname_file.close()

	nationality_file = open("nationalities.txt", "r")
	nationality_list = nationality_file.read().splitlines()
	nationality_file.close()

	line_num = 1
	for l in sys.stdin:
		fields = l.split("\t")
		if fields[1] == "artist":
			process_artist(fields, str(line_num))
		elif fields[1] == "person":
			process_person(fields, str(line_num))
		elif fields[1] == "location":
			process_location(fields, str(line_num))
		elif fields[1] == "artwork":
			process_artwork(fields, str(line_num))
		elif fields[1] == "event":
			process_event(fields, str(line_num))
		elif fields[1] == "visual_art_form":
			process_art_form(fields, str(line_num))
		elif fields[1] == "visual_art_genre":
			process_art_genre(fields, str(line_num))
		elif fields[1] == "art_period_movement":
			process_art_period_movement(fields, str(line_num))
		elif fields[1] == "visual_art_medium":
			process_visual_art_medium(fields, str(line_num))
		elif fields[1] == "nationality":
			process_nationality(fields, str(line_num))
		elif fields[1] == "museum":
			process_museum(fields, str(line_num))
		elif fields[1] == "mythology":
			process_mythology(fields, str(line_num))
		elif fields[1] == "family":
			process_family(fields, str(line_num))
		elif fields[1] == "group":
			process_group(fields, str(line_num))
		line_num += 1

	for item in dictionary.items():
		print item[0] + "\t" + ";".join(item[1])

