import itertools
import os
import unicodedata
import re
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

dictionary = {}

######################################################################
def remove_accent(input_str):
	nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
	return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

######################################################################
def add_artist_to_dictionary(_key, _value):
	if len(re.findall(r"[0-9]+[^snrt]", _key)) == 0:
		add_to_dictionary(remove_accent(_key), _value, "a:")
		add_to_dictionary(_key, _value, "a:")

######################################################################
def add_to_dictionary(_key, _value, _prefix):
	unsuitable = ";?!()[]{}<>/~@#$%^&*_=+|\"\\"
	_key = _key.strip()
	for x in unsuitable:
		if x in _key:
			return
	if _key != "":
		_key = _prefix+"\t"+_key
		if _key not in dictionary:
			dictionary[_key] = set()
		dictionary[_key].add(_value)

######################################################################
def process_location(_l, _line_num):
	aliases = _l[3].split("|")
	add_to_dictionary(_l[2], _line_num, "l:")
	if "County" in _l[2]:
		_l[2] = re.sub(r" County$", "", _l[2])
		_l[2] = re.sub(r"^County (?=[A-Z])", "", _l[2])
		add_to_dictionary(_l[2], _line_num, "l:")

	for t in aliases:
		add_to_dictionary(t, _line_num, "l:")

######################################################################
def process_artwork(_a, _line_num):
	aliases = _a[3].split("|")
	add_to_dictionary(_a[2], _line_num, "w:")
	for t in aliases:
		add_to_dictionary(t, _line_num, "w:")

######################################################################
def add_alternatives(_name, _line_num):
	if "Mc" in _name:
		f_alter = _name
		f_new = re.sub(r"Mc([A-Z])", "Mc \g<1>", f_alter) # McCollum -> Mc Collum
		add_artist_to_dictionary(f_new, _line_num)
		f_alter = _name
		f_new = re.sub(r"Mc ([A-Z])", "Mc\g<1>", f_alter) # Mc Collum -> McCollum
		add_artist_to_dictionary(f_new, _line_num)
	if "." in _name:
		f_alter = _name
		f_new = re.sub(r"([A-Z])\. (?=[A-Z])", "\g<1>.", f_alter) # J. M. W. Turner -> J.M.W.Turner
		add_artist_to_dictionary(f_new, _line_num)
		f_alter = f_new
		f_new = re.sub(r"([A-Z])\.(?=[A-Z][a-z]+)", "\g<1>. ", f_alter) # J.M.W.Turner -> J.M.W. Turner
		add_artist_to_dictionary(f_new, _line_num)
		f_alter = f_new
		f_new = re.sub(r"\.", "", f_alter) # J.M.W. Turner -> JMW Turner
		add_artist_to_dictionary(f_new, _line_num)
	if "-" in _name:
		f_alter = _name
		f_new = re.sub(r"\-", " ", f_alter) # Payne-John Christo -> Payne John Christo
		add_artist_to_dictionary(f_new, _line_num)

######################################################################
def process_artist(_p, _line_num):

	display_term = re.sub('\s+', ' ', _p[2]).strip()
	preferred_term = re.sub('\s+', ' ', _p[3]).strip()
	aliases = _p[4].split("|")

	f_result = display_term
	for x in f_name:
		if x in f_result:
			f_result = re.sub(' ?,? '+x+'$', '', f_result)
			f_result = re.sub('^'+x+' ', '', f_result)
			add_artist_to_dictionary(f_result, _line_num)

	if display_term in allow_list:
		add_artist_to_dictionary(display_term, _line_num)

	if "," not in display_term:
		add_alternatives(display_term, _line_num)
		add_artist_to_dictionary(display_term, _line_num)
		length = display_term.count(" ") + 1
		if length <= 4 and length > 1:
			parts = display_term.split(" ")
			names = list(itertools.permutations(parts))
			for x in names:
				r = " ".join(x)
				add_alternatives(r, _line_num)

	length = preferred_term.count(" ") + 1
	if "," not in preferred_term:
		add_alternatives(preferred_term, _line_num)
		add_artist_to_dictionary(preferred_term, _line_num)
		if length <= 4 and length > 1:
			parts = preferred_term.split(" ")
			names = list(itertools.permutations(parts))
			for x in names:
				r = " ".join(x)
				add_artist_to_dictionary(r, _line_num)
	else:
		parts = preferred_term.split(",")
		for p in parts:
			p = p.strip()
			if p not in f_name and length <= 2:
				add_artist_to_dictionary(p, "N")

	for t in aliases:
		t = re.sub('\s+', ' ', t).strip()
		if "," not in t:
			add_alternatives(t, _line_num)
			add_artist_to_dictionary(t, _line_num)
			length = t.count(" ") + 1
			if length <= 4 and length > 1:
				parts = t.split(" ")
				names = list(itertools.permutations(parts))
				for x in names:
					r = " ".join(x)
					add_artist_to_dictionary(r, _line_num)

			if t in display_term and t != display_term and length == 1:
				add_artist_to_dictionary(t, "N")
			if t in preferred_term and t != preferred_term and length == 1:
				add_artist_to_dictionary(t, "N")

#####################################################################
def process_event(_e, _line_num):
	aliases = _e[3].split("|")
	add_to_dictionary(_e[2], _line_num, "e:")
	for t in aliases:
		add_to_dictionary(t, _line_num, "e:")

#####################################################################
def process_art_form(_f, _line_num):
	aliases = _f[3].split("|")
	add_to_dictionary(_f[2], _line_num, "f:")
	for t in aliases:
		add_to_dictionary(t, _line_num, "f:")

#####################################################################
def process_art_genre(_g, _line_num):
	aliases = _g[3].split("|")
	add_to_dictionary(_g[2], _line_num, "g:")
	for t in aliases:
		add_to_dictionary(t, _line_num, "g:")

#####################################################################
def process_art_period_movement(_m, _line_num):
	aliases = _m[3].split("|")
	add_to_dictionary(_m[2], _line_num, "m:")
	for t in aliases:
		add_to_dictionary(t, _line_num, "m:")

#####################################################################
def process_visual_art_medium(_d, _line_num):
	aliases = _d[3].split("|")
	add_to_dictionary(_d[2], _line_num, "d:")
	for t in aliases:
		add_to_dictionary(t, _line_num, "d:")

#####################################################################
def process_nationality(_n, _line_num):
	aliases = _n[8].split("|")
	for t in aliases:
		add_to_dictionary(t, _line_num, "n:")

#####################################################################
def process_museum(_m, _line_num):
	aliases = _m[3].split("|")
	add_to_dictionary(_m[2], _line_num, "c:")
	for t in aliases:
		add_to_dictionary(t, _line_num, "c:")

#####################################################################
def process_mythology(_c, _line_num):
	aliases = _c[3].split("|")
	aliases.append(_c[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "y:")

#####################################################################
def process_family(_c, _line_num):
	aliases = _c[3].split("|")
	aliases.append(_c[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "i:")

#####################################################################
def process_group(_c, _line_num):
	aliases = _c[3].split("|")
	aliases.append(_c[2])
	for t in aliases:
		add_to_dictionary(t, _line_num, "r:")

#####################################################################
def process_person(_m, _line_num):
	aliases = _m[3].split("|")
	add_to_dictionary(_m[2], _line_num, "p:")
	for t in aliases:
		add_to_dictionary(t, _line_num, "p:")

#####################################################################
if __name__ == "__main__":
	f_file = open("freq_terms_filtred.all", "r")
	f_name = f_file.read().splitlines()
	f_file.close()

	allow_file = open("allow_list", "r")
	allow_list = allow_file.read().splitlines()
	allow_file.close()

	line_num = 1
	for l in sys.stdin:
		fields = l.split("\t")
		if fields[1] == "artist":
			process_artist(fields, str(line_num))
		elif fields[1] == "person":
			process_person(fields, str(line_num))
		elif fields[1] == "location":
			process_location(fields, str(line_num))
		elif fields[1] == "artwork":
			process_artwork(fields, str(line_num))
		elif fields[1] == "event":
			process_event(fields, str(line_num))
		elif fields[1] == "visual_art_form":
			process_art_form(fields, str(line_num))
		elif fields[1] == "visual_art_genre":
			process_art_genre(fields, str(line_num))
		elif fields[1] == "art_period_movement":
			process_art_period_movement(fields, str(line_num))
		elif fields[1] == "visual_art_medium":
			process_visual_art_medium(fields, str(line_num))
		elif fields[1] == "nationality":
			process_nationality(fields, str(line_num))
		elif fields[1] == "museum":
			process_museum(fields, str(line_num))
		elif fields[1] == "mythology":
			process_mythology(fields, str(line_num))
		elif fields[1] == "family":
			process_family(fields, str(line_num))
		elif fields[1] == "group":
			process_group(fields, str(line_num))
		line_num += 1

	for item in dictionary.items():
		print item[0] + "\t" + ";".join(item[1])

