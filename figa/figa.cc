/*
Copyright 2014 Brno University of Technology

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

//=====================================================//
// Soubor: figa.cc v0.8                                //
// Autor: Marek Visnovsky,  xvisno00@stud.fit.vutbr.cz //
// Upravil: Karel Brezina,  xbrezi13@stud.fit.vutbr.cz //
// Upravil: Peter Hostacny, xhosta03@stud.fit.vutbr.cz //
//=====================================================//

#include <ctype.h>
#include <cmath>
#include "figa.h"

// Trida marker

//================================//
// Nazev: initList                //
// Trida: marker                  //
// Popis: pocatecni inicializace  //
//================================//
void marker::initList() {
	ulist.First = NULL;
	ulist.Act = NULL;
	ulist.Move = NULL;
} // konec initList

//=================================================//
// Nazev: insertItem                               //
// Trida: marker                                   //
// Popis: provede vlozeni nove polozky do seznamu  //
//=================================================//
void marker::insertItem(unsigned int offset, unsigned int value, unsigned long int char_value) {
	TListptr* item;
	item = (TListptr*) malloc(sizeof(struct ListItem));
	item->char_value = char_value;

	if (ulist.First == NULL) { // je pridavana polozka prvni?
		ulist.First = item;
		ulist.Act = item;
		ulist.Move = item;
		item->count_sub = value;
		item->offset_id = offset;
		item->next = NULL;
	}
	else {
		ulist.Move->next = item;
		item->count_sub = ulist.Move->count_sub + value;
		item->offset_id = offset;
		item->next = NULL;
		ulist.Move = item;
	}
	
} // konec insertItem

//===========================================//
// Nazev: deleteItems                        //
// Trida: marker                             //
// Popis: vymaze vsechny polozky v seznamu   //
//===========================================//
void marker::deleteItems() {
	TListptr* help;
	while (ulist.First != NULL) { // je posledni polozka?
		help = ulist.First;
		ulist.First = ulist.First->next;
		free(help);
	}
	ulist.Act = NULL;
	ulist.Move = NULL;
} // konec marker

// Trida data_proc

//==========================================//
// Nazev: get_line                          //
// Trida: data_proc                         //
// Popis: nacitaci sekvence pro hash cisla  //
//==========================================//
void data_proc::save_line(unsigned long int radek, bool end) {
	std::stringstream string_tmp;
	if (end) { // je cislo radku posledni?
		string_tmp << radek;
		radek_line.append(string_tmp.str());
	}
	else {
		string_tmp << radek << ';';
		radek_line.append(string_tmp.str());
	}
} // konec data_proc::get_line

//=======================================//
// Nazev: get_data                       //
// Trida: data_proc                      //
// Popis: vypis dat o entite do stringu  //
//=======================================//
void data_proc::get_data(Tmatch match) {
	std::stringstream string_tmp;
	string_tmp << match.line_num << '\t' << match.first_index << '\t' << match.last_index << '\t' << match.line_name << std::endl;
	results.append(string_tmp.str()); // ulozeni nactenych dat
} // konec data_proc::get_data

// Trida marker

//=================================================//
// Nazev: to_skip                                  //
// Trida: marker                                   //
// Popis: urcuje, ktere znaky se maji preskakovat  //
//=================================================//
bool marker::to_skip(unsigned long int znak) {
	switch(znak) {
		case ' ':
		case '\"':
		case '\'':
		case '.':
		case ',':
		case '\0':
		case '\t':
		case 0xE2809D: // right double quotation mark
		case 0xE28093: // en dash
		case 0xE2809C: // left double quotation mark
			return true; // preskocit
		default:
			return false; // nepreskakovat
	}
} // konec marker::to_skip

//========================================================//
// Nazev: set_index                                       //
// Trida: marker                                          //
// Popis: nastavi index udavajici pozici v texte na nulu  //
//========================================================//
void marker::set_index() {
	index = -1;
	first = 0;
	line_index = 0;
	a = ' ';
	b = ' ';
	index_global = 0;                // reset
	line = "";
	first_par = true;
	was_end = false;

	initList(); // inicializace seznamu pro index utf-8 znaku

} // konec marker::set_index

//==================================================//
// Nazev: dec2hex                                   //
// Trida: marker                                    //
// Popis: prevede decimalni cislo na hexadecimalni  //
//==================================================//
void marker::dec2hex(char *ext, unsigned int znak) {
	unsigned int kolik = 0;
	unsigned int hex = 0;
	for (int i = 1; i > -1; i--) {
		kolik = znak/16;
		hex = znak%16;
		znak = kolik;
		if (hex < 10) { // rozsah (0-9)
			ext[i] = hex + '0';
		}
		else { // rozsah (A-F)
			ext[i] = hex + 'A' - 10;
		}
	}
} // konec marker::dec2hex

//==================================================//
// Nazev: hex2dec                                   //
// Trida: marker                                    //
// Popis: prevede hexadecimalni znak na cislo	    //
//==================================================//
int marker::hex2dec(char ext) {
	
		if (ext >= 'A' && ext <= 'F') { 
			return ext - 'A' + 10;
		}
		else {
			return ext - '0';
		}
} // konec marker::hex2dec

//===================================================//
// Nazev: utf2symbols                                //
// Trida: marker                                     //
// Popis: prevede utf-8 znaky na ascii reprezentaci  //
//===================================================//
std::string marker::utf2symbols(std::string line) {
	unsigned int count = 0;
	unsigned int index = 0;
	unsigned int neco = line[0];
	unsigned int pocet = 0;

	char pole[2];
	char* buf;
	buf = &pole[0];

	char buffer;
	bool now = true;
	std::string out = "";
	unsigned long int char_value = 0;

	while (neco != '\0') {
		char_value = 0;

		if ((neco >= 0) && (neco < 128)) { // je znak v rozsahu
			buffer = neco;                  // zakladni ASCII sady
			out.append(1,buffer); // pridani do vystupniho retezce
			count++;
			index++;
			neco = line[count]; // nacteni dalsiho znaku
		}
		else {
			out.append("&#x"); // predpona utf-8 znaku
			index += 3;
			pocet = 2;

			for (int i = 6; i > 0; i--) { // na kolik bytu je utf-8 znak
				if (!((neco >> i) & 1)) {
					pocet = 7 - i;
					break;
				}
			}		

			for (int i = 0; i < pocet; i++) {

				dec2hex(buf, neco); // prevod na decimalni hodnotu
				for (int j = 0; j < 2; j++) {
					out.append(1,pole[j]); // pridani do vystupniho retezce
					char_value += hex2dec(pole[j]) * pow(16.0, (pocet * 2 - 1) - i * 2 - j);
					index++;
				}

				count++;
				neco = line[count]; // nacteni dalsi casti utf-8 znaku
			}

			out.append(";"); // pripona utf-8 znaku
			index++;
			insertItem(index, (pocet*2)+3, char_value); // zaindexovani konce utf-8
		}                                  // znaku
	}

	ulist.Move = ulist.First; // posunutí indexu

	return out;
} // konec utf2symbols

//=====================================//
// Nazev: next_char                    //
// Trida: marker                       //
// Popis: nacita dalsi znak ze vstupu  //
//=====================================//
void marker::next_char() {
	// na konci radku nacitam dalsi
	if (line.empty() || line_index == line.length()+1) { // konec radku?
		if (ulist.Move != NULL) { // zacatek nacitani?
			index_global += line.length() + 1 - ulist.Move->count_sub;
		} // index konce predchoziho radku
		else {
			index_global = 0; // zacatek
		}
		deleteItems(); // reset seznamu indexu utf-8 znaku
		insertItem(1,0,0); // nastaveni pocatecni hodnoty
		line_index = 0; // reset
		getline(*input, line); // nacteni noveho radku
		line = utf2symbols(line); // prevod utf-8 na ascii
		index_s = 0;
		first_par = true; // prvni slovo?
	}
	// nacitani znakupri
	if (!input->eof()) {

		b = a; // predchozi znak
		a = line[line_index]; // akt. znak
		line_index ++;

		if (a == ';') { // je znak konec utf-8 znaku?
			if (ulist.Move->next != NULL) { // existuje dalsi utf-8 znak?
				ulist.Move = ulist.Move->next; // posun v seznamu na dalsi
			}                                 // index utf-8 znaku
		}

		if (!to_skip(a) && to_skip(b) && first_par) { //
			index_next = line.find_first_of(" ",index_s+1); // index pro
			first_par = false;                           // backtracking
		}

		if (index_next == std::string::npos) { // neexistuje dalsi slovo?
			index_next = 0; // reset
			first_par = true; // reset
		}

		index++;
	}
} // konec marker::next_char

//==========================================================//
// Nazev: next_word                                         //
// Trida: marker                                            //
// Popis: preskoci na zacatek nasledujiciho vyrazu v texte  //
//==========================================================//
void marker::next_word() {
	index++;
	ulist.Move = ulist.Act; // nastaveni indexu na akt. polozku

	while (((iscntrl(a) || isspace(a) || to_skip(a)) && !input->eof())) {
		//cout << "Index: " << index_s << " ulist.move: " << ulist.Act->offset_id << " value: " << ulist.Act->char_value << endl;
			next_char(); // dalsi znak
			index_s++;
			index--;

			while (ulist.Act->next != NULL && to_skip(ulist.Act->next->char_value) && line_index == ulist.Act->next->offset_id - ulist.Act->next->count_sub) { // je treba se

				ulist.Act = ulist.Act->next;                  // posunout?
				ulist.Move = ulist.Act;                     // index utf-8

				if (ulist.Act->next == NULL) {             // znaku musi byt
					break;                                  // pred line_index
				}

				for (int i = ulist.Act->count_sub; i >= 0; i--) {
					next_char();
					index_s++;
					//index--;
				}

				index--;
			}
	}

	if (!begin) { // neni zacatek vyhledavani?
		index_s--;
	}

	begin = false;

	if (ulist.Act->next != NULL) { // existuje dalsi utf-8 znak?
		while (line_index > ulist.Act->next->offset_id) { // je treba se

			ulist.Act = ulist.Act->next;                  // posunout?
			ulist.Move = ulist.Act;                     // index utf-8

			if (ulist.Act->next == NULL) {             // znaku musi byt
				break;                                  // pred line_index
			}
		}
	}

	if (ulist.Act != NULL) { // aktualizovat index zacatku slova?
		first = index_global + index_s + 1 - ulist.Act->count_sub;
	}
} // konec marker::next_word

//======================================//
// Nazev: end_word                      //
// Trida: marker                        //
// Popis: skoci na konec slova v texte  //
//======================================//
void marker::end_word() {
	while (((!isspace(a) && !iscntrl(a)) || a == '\\') && !input->eof()) {
		next_char(); // nacti dalsi znak
	}
	if (((line.length()+1) == line_index) && first_par) { // jsme na
		was_end = true;                               // konci radku?
	}
} // konec marker::end_word

//======================================================//
// Nazev: save_match                                    //
// Trida: marker                                        //
// Popis: ulozi nalezeny vyraz do promenne typu Tmatch  //
//======================================================//
Tmatch marker::save_match(unsigned long int f_index, unsigned long int l_index, std::string radek_line, std::string jmeno) {
	Tmatch tmp;
	tmp.first_index = f_index; // index zacatku slova
	tmp.last_index = l_index; // index konce slova
	tmp.line_num = radek_line; // seznam cisel radku
	tmp.line_name = jmeno; // nazev entity
	return tmp;
} // konec marker::save_match

//=======================================//
// Nazev: lookup                         //
// Trida: marker                         //
// Popis: vyhledavani ve vstupnim texte  //
//=======================================//
std::string marker::lookup() {
	dict_list *my_dict;
	fsa_arc_ptr *dummy = NULL;

	my_dict = &dictionary;
	ANNOT_SEPARATOR = my_dict->item()->annot_sep;
	set_dictionary(my_dict->item());

	index_next = 0;

	set_index(); // reset
	begin = true; // reset
	next_char(); // nacti prvni znak

	while(!input->eof()) { // je konec?
		next_word(); // dalsi slovo
		match_word(dummy->first_node(current_dict));
		// zavola se vyhledavaci funkce
		if (first_par && !was_end) { // prvni slovo?
			index_next = line.find_first_of(" ",index_s+1);
		}
		// inkrementace a reset
		if (was_end) {
			next_char();
		}
		line_index = index_next + 1;
		index = 0;
		first_par = true;
		a = ' ';
		index_s = index_next + 1;
		if (was_end) {
			was_end = false;
			line_index = 0;
			index_s = 0;
		}
	}
	deleteItems(); // odstran vsechny polozky ze seznamu

	return dataset.results;
} // konec marker::lookup

//=======================================//
// Nazev: lookup_file                    //
// Trida: marker                         //
// Popis: vyhledavani ve vstupnim texte  //
//=======================================//
std::string marker::lookup_file(std::string input_name) {
	set_input_filename(input_name);
	return lookup();
}

//=======================================//
// Nazev: lookup_string                  //
// Trida: marker                         //
// Popis: vyhledavani ve vstupnim texte  //
//=======================================//
std::string marker::lookup_string(std::string input_string) {
	std::stringstream ss;
	ss.str(input_string);

	set_input(&ss);
	return lookup();
}

//================================//
// Nazev: set_input_filename      //
// Trida: marker                  //
// Popis: otevre vystupni soubor  //
//================================//
bool marker::set_input_filename(std::string name) {
	if (dataset.input_file.is_open()) dataset.input_file.close();
	dataset.input_file.open(name.c_str());

	if (!dataset.input_file.is_open()) // podarilo se nacist soubor?
	{
		std::cerr << "FIGA ERROR: Intput file \"" << name << "\" cannot be opened" << std::endl;
		return false;
	}

	set_input(&dataset.input_file);

	return true;
} // konec marker::set_input_filename


//========================================================//
// Nazev: set_input                                      //
// Trida: marker                                          //
// Parameter: input										    //
// Popis: nastavuje vstup									//
//========================================================//
void marker::set_input(std::istream* in) {
	input = in;
}

//========================================================//
// Nazev: match_word                                      //
// Trida: marker                                          //
// Parameter: ukazovatel pro fsa, na zacatek automatu;    //
// Popis: prochazi znak po znaku automatem a hleda shodu  //
//========================================================//
void marker::match_word(fsa_arc_ptr start) {
	bool found; // byla nalezena shoda znaku se zaznamem v automatu?
	bool frag = false; // je match fragment?
	bool fin = false; // je konec?
	bool save_mod = false; // je ulozen aspon jeden zaznam?
	unsigned int radek = 0; // info o radku v KB
	std::string jmeno; // ukladani nazvu entity
	jmeno.clear();
	fsa_arc_ptr test; // docasne ulozeny uzel

	do {
		fsa_arc_ptr next_node = start.set_next_node(current_dict);
		found = false;
		forallnodes(i) { // hledani shody znaku s automatem
			if (a == next_node.get_letter()) {
				jmeno.append(1,a);
				found = true;
				start = next_node; // dalsi uzel
				break;
			}
		}

		if (!found && fin) { // konec

			end_word();

			if (save_mod) {
				dataset.get_data(potencial_match);
			}
			return;
		}
		else if (!found) {
			end_word();

			if (save_mod) {
				dataset.get_data(potencial_match);
			}
			return;
		}
	// nacti dalsi znak
	next_char();

	test = start; // ulozeni uzlu
	next_node = start.set_next_node(current_dict);

	forallnodes(i)
	{ // existuje zaznam v automatu
		if (('|' == next_node.get_letter()) && (ispunct(a) || isspace(a) || iscntrl(a) || input->eof()))
		{
			fin = true; // zacni s hledanim cisel radku
			break;
		}
	}

	if (fin) // byl najity zaznam v automatu, ted se bude rozpoznavat
    	{
		dataset.radek_line.clear();                  // vsech radku v KB
		start = next_node;
		next_node = start.set_next_node(current_dict);
		start = next_node;
		radek = 0; // temp pro hodnotu radku
		char znak = next_node.get_letter(); // prvni znak za '|'
		bool no_number = false; // nema cislo radku?
		bool run = true; // ma zacit hledat?
		int bitshift = 21;      // bitovy posuv kvoli kodovaniu cisla v KA
		int byte_number = 4;    // cislo riadku je v KA zapisane na 4 bajtoch

		if (znak == '\n') { // chybny format?
			dataset.save_line(radek, true);
			potencial_match = save_match(first, (first+index-2), dataset.radek_line, jmeno);
			save_mod = true;
			fin = false;
			run = false;
		}

		while (run) // ma se zacit vyhledavat?
        {
            if ((byte_number > 0) &&(znak != 'N'))
            {
                radek |= (((int)(znak & 0xFE)) << bitshift) >> 1;
				// odfiltrovanie spodneho bitu a priradenie bajtu do cisla
                bitshift -= 7;
                byte_number--;  // posun na dalsi byte
            }

			if (znak == 'N')    // ('N' = 78 in ASCII)
            { // fragment entity?
				no_number = true;
				frag = true;
			}

			fsa_arc_ptr next_node = start.set_next_node(current_dict);
			start = next_node;
			znak = next_node.get_letter(); // dalsi znak v uzlu

			if ((byte_number == 0) &&(znak%2 == 0x01)) // ak prave nacitany znak patri uz do dalsieho cisla
            {
                dataset.save_line(radek, false);
                radek = 0;
				byte_number = 4;
				bitshift = 21;
			}

			if ((znak == '\0') || (znak == '\n')) // posledne cislo v riadku?      ('\0' = 0 ... '\n' = 10 in ASCII)
            {
				if ((radek != 0) || (no_number))
				{
					dataset.save_line(radek, true);
					potencial_match = save_match(first, (first+index-2) - (ulist.Move->count_sub - ulist.Act->count_sub), dataset.radek_line, jmeno);	// ulozeni info o entite do temp struktury
					save_mod = true;
					fin = false;
				}
				break;
			}
		}
	}

	if (fin && input->eof())
		dataset.get_data(potencial_match); // uloz zaznam do stringu

	next_node = test; // navraceni se na puvodni uzel
	start = test;

	} while (!input->eof() && found);

} // konec marker::match_word
