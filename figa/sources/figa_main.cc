/*
Copyright 2014 Brno University of Technology

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

//=====================================================//
// Soubor: figa_main.cc v0.8                           //
// Autor: Marek Visnovsky, xvisno00@stud.fit.vutbr.cz  //
// Upravil: Karel Brezina, xbrezi13@stud.fit.vutbr.cz  //
// Upravil: Peter Hostacny, xhosta03@stud.fit.vutbr.cz //
// Popis: vyhledavani entit ve fulltextu               //
//=====================================================//

#include <iostream>
#include <fstream>
#include <unistd.h>

#include "fsa.h"
#include "nstr.h"
#include "common.h"
#include "figa.h"
#include "spell.h"

//====================================//
// Nazev: print_help                  //
// Popis: vytiskne napovedu na stdin  //
//====================================//
void print_help () {
	std::cout << std::endl;
	std::cout << "updated: FItGAzetter v0.8, July 9th, 2014, Peter ";
	std::cout << "Hostacny, xhosta03@stud.fit.vutbr.cz" << std::endl;
	std::cout << "updated: FItGAzetter v0.7c, November 16th, 2013, Karel ";
	std::cout << "Brezina, xbrezi13@stud.fit.vutbr.cz" << std::endl;
	std::cout << "FItGAzetteer v0.35c, September 14th, 2010, Marek ";
	std::cout << "Visnovsky, xvisno00@stud.fit.vutbr.cz" << std::endl;
	std::cout << "based on: fsa Ver. 0.49, March 18th, 2009, (c) Jan Daciuk";
	std::cout << ",jandac@eti.pg.gda.pl" << std::endl << std::endl;
	std::cout << "Usage: ./figav08 [options]..." << std::endl << std::endl;
	std::cout << "Options:" << std::endl;
	std::cout << "  -a        -> autocomplete function" << std::endl;
	std::cout << "  -b        -> return offset in bytes [ONLY FIGA]" << std::endl;
	std::cout << "  -d FILE   -> define automaton file" << std::endl;
	std::cout << "  -f FILE   -> define input file" << std::endl;
	std::cout << "  -h        -> print this help" << std::endl;
	std::cout << "  -m NUMBER -> define number of returned entities [ONLY AUTOCOMPLETE]" << std::endl;
	std::cout << "               (default value is 5)" << std::endl;
	std::cout << "  -o        -> turn on entity overlapping [ONLY FIGA]" << std::endl;
	std::cout << "  -p        -> for print out string" << std::endl;
	return;
} // konec print_help

//=========================//
// Nazev: main             //
// Popis: spousti program  //
//=========================//
int main (const int argc, char *argv[]) {

	word_list dict;
	const char *lang_file = NULL;
	char *fsa_name = NULL; // nazev konecneho automatu
	char *file_name = NULL; // nazev souboru, ze ktereho se ctou data
	std::string name_file;
	int c, error = 1;
	char* temp = NULL;
	unsigned int many = 5;
	bool file = false; // cteni dat ze souboru?
	bool print_bool = false; // tisk na stdin?
	int xx = 0;
	char znak = '0';
	bool in_bytes = false;  // vraciat offset v znakoch/bytoch - podla argumentu '-b'
	//std::cout << "inBytes = TRUE" << std::endl;
	bool overlapping = false;  // vraciat prekryvajuce sa entity 
	bool autocomplete = false;	// funkcionalita autocomplete
	// autocomplete = vratenie zadaneho poctu (-m) entit

	// zpracovani argumentu
	while ((c = getopt(argc, argv, "abod:f:phm:")) != -1)
		switch (c) {
			case 'a':
				autocomplete = true;
				break;
			case 'b':
				in_bytes = true;
				break;
			case 'o':
				overlapping = true;
				break;
			case 'd':
				fsa_name = optarg;
				error--;
				break;
			case 'f':
				file = true;
				file_name = optarg;
				name_file = file_name;
				break;
			case 'p':
				print_bool = true;
				break;
			case 'h':
				error = 2;
				break;
			case 'm':
				temp = optarg;
				many = 0;
				znak = '0';
				xx = 0;

				while (temp[xx] != '\0') {
					znak = temp[xx];
					while (isdigit(znak)) {
						for (int i = 0; i < 10; i++) {
							if ((i+'0') == znak) {
								many *= 10;
								many += i;
								break;
							}
						}
						break;
					}
					xx++;
				}

				if (many == 0) {
					many = 300000;
				}

				break;
			default:
				std::cerr << "FIGA ERROR: Unrecognized option." << std::endl;
				error = 2;
			}

	// osetreni chybejicich argumentu
	if (error > 0) {
		if (error != 2)
			std::cout << "FIGA ERROR: One dictionary file must be specified!" << std::endl;
		else
			print_help(); // tisk help
		return 1;
	}
	if (many > 300000 || many <= 0)	{
		std:cerr << "FIGA ERROR: Argument '-m' must be nonnegative number (<= 300000)." << std::endl;
		return 4;
	}

	// overenie existencie automatu
	std::ifstream soubor(fsa_name);
	if (!soubor.is_open()) { // chyba pri otvarani automatu
		std::cerr << "FIGA ERROR: Automata file couldn't be opened!" << std::endl;
		return 2;
	}
	soubor.close();

	if (file) {  // bol zadany prepinac -f ?
		std::ifstream input_file(file_name);
		if (!input_file.is_open()) { // chyba pri otvarani vstupneho suboru
			std::cerr << "FIGA ERROR: Intput file \"" << file_name << "\" couldn't be opened!" << std::endl;
			return 3;
		}
		input_file.close();
	}

	dict.insert(fsa_name); // nacteni slovniku (automatu)
	marker seek_names(&dict, lang_file, in_bytes, overlapping); // inicializace slovniku

	if (file) 	seek_names.set_input_filename(file_name);
	else 		seek_names.set_input(&std::cin);


	if (autocomplete) {    // AUTOCOMPLETE
		if (print_bool) // tisk na cout?
			std::cout << seek_names.auto_lookup(many);
		else  // bez tisku na cout
			seek_names.auto_lookup(many);
	}
	else {  // FIGA
		if (print_bool) // tisk na cout?
			std::cout << seek_names.lookup();
		else  // bez tisku na cout
			seek_names.lookup();
	}

	return 0;
} // konec main
