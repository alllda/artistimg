%module marker
%{
#include "figa.h"
#include "fsa.h"
#include "common.h"
#include "nstr.h"
%}

%include "std_string.i"

template <class T>
class list {
public:
  list(void);
  void insert(const T *new_item);
};

%template(myList) list<char>;

typedef list<char> word_list;
  
class marker : public fsa {   
public:      
    marker(word_list *dict_names, const char *language_file = NULL, bool in_bytes = false,
			bool overlapping = false) : fsa(dict_names, language_file) 
			{
				offset_in_bytes = in_bytes;
				entity_overlapping = overlapping;
			} // konstruktor
    std::string lookup();              
	std::string lookup_file(std::string input_name);
	std::string lookup_string(std::string input_string);

	// AUTOCOMPLETE:
	std::string auto_lookup(unsigned int many);
	std::string auto_lookup_file(std::string input_name, unsigned int many);
	std::string auto_lookup_string(std::string input_string, unsigned int many);
};
