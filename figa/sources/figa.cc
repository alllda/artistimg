/*
Copyright 2014 Brno University of Technology

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

//=====================================================//
// Soubor: figa.cc v0.8                                //
// Autor: Marek Visnovsky,  xvisno00@stud.fit.vutbr.cz //
// Upravil: Karel Brezina,  xbrezi13@stud.fit.vutbr.cz //
// Upravil: Peter Hostacny, xhosta03@stud.fit.vutbr.cz //
//=====================================================//

#include <ctype.h>
#include <cmath>
#include "figa.h"
#include <list>

// Trida marker

//================================//
// Nazev: initList                //
// Trida: marker                  //
// Popis: pocatecni inicializace  //
//================================//
void marker::initList() {
	ulist.First = NULL;
	ulist.Act = NULL;
	ulist.Move = NULL;
} // konec initList

//=================================================//
// Nazev: insertItem                               //
// Trida: marker                                   //
// Popis: provede vlozeni nove polozky do seznamu  //
//=================================================//
void marker::insertItem(unsigned int offset, unsigned int value, unsigned long int char_value) {
	TListptr* item;
	item = (TListptr*)malloc(sizeof(struct ListItem));
	item->char_value = char_value;

	if (ulist.First == NULL) { // je pridavana polozka prvni?
		ulist.First = item;
		ulist.Act = item;
		ulist.Move = item;
		item->count_sub = value;
		item->offset_id = offset;
		item->next = NULL;
	}
	else {
		ulist.Move->next = item;
		item->count_sub = ulist.Move->count_sub + value;
		item->offset_id = offset;
		item->next = NULL;
		ulist.Move = item;
	}

} // konec insertItem

//===========================================//
// Nazev: deleteItems                        //
// Trida: marker                             //
// Popis: vymaze vsechny polozky v seznamu   //
//===========================================//
void marker::deleteItems() {
	TListptr* help;
	while (ulist.First != NULL) { // je posledni polozka?
		help = ulist.First;
		ulist.First = ulist.First->next;
		free(help);
	}
	ulist.Act = NULL;
	ulist.Move = NULL;
} // konec marker

// Trida data_proc

//==========================================//
// Nazev: get_line                          //
// Trida: data_proc                         //
// Popis: nacitaci sekvence pro hash cisla  //
//==========================================//
void data_proc::save_line(unsigned long int radek, bool end) {
	std::stringstream string_tmp;
	if (end) { // je cislo radku posledni?
		string_tmp << radek;
		radek_line.append(string_tmp.str());
	}
	else {
		string_tmp << radek << ';';
		radek_line.append(string_tmp.str());
	}
} // konec data_proc::get_line

//=======================================//
// Nazev: get_data                       //
// Trida: data_proc                      //
// Popis: vypis dat o entite do stringu  //
//=======================================//
void data_proc::get_data(Tmatch match, int choice) {
	std::stringstream string_tmp;
	if (choice == AUTOCOMPLETE)
		string_tmp << match.line_name << '\t' << match.line_num << endl;
	else
		string_tmp << match.line_num << '\t' << match.first_index << '\t' << match.last_index << '\t' << match.line_name << std::endl;
	results.append(string_tmp.str()); // ulozeni nactenych dat
} // konec data_proc::get_data

// Trida marker

//=================================================//
// Nazev: to_skip                                  //
// Trida: marker                                   //
// Popis: urcuje, ktere znaky se maji preskakovat  //
//=================================================//
bool marker::to_skip(unsigned long int znak) {

	if (strchr(delimiters, znak))
		return true;

	switch (znak) {
	//case ' ':
	//case '\"':
	//case '\'':
	//case '.':
	//case ',':
	//case '\t':
	//case '\0':
	case 0xE2809D: // right double quotation mark
	case 0xE28093: // en dash
	case 0xE2809C: // left double quotation mark
		return true; // preskocit
	default:
		return false; // nepreskakovat
	}
} // konec marker::to_skip

//========================================================//
// Nazev: set_index                                       //
// Trida: marker                                          //
// Popis: nastavi index udavajici pozici v texte na nulu  //
//========================================================//
void marker::set_index() {
	index = -1;
	first = 0;
	line_index = 0;
	a = ' ';
	b = ' ';
	index_global = 0;                // reset
	line.clear();
	first_par = true;
	was_end = false;

	initList(); // inicializace seznamu pro index utf-8 znaku

} // konec marker::set_index

//==================================================//
// Nazev: dec2hex                                   //
// Trida: marker                                    //
// Popis: prevede decimalni cislo na hexadecimalni  //
//==================================================//
void marker::dec2hex(char *ext, unsigned int znak) {
	unsigned int kolik = 0;
	unsigned int hex = 0;
	for (int i = 1; i > -1; i--) {
		kolik = znak / 16;
		hex = znak % 16;
		znak = kolik;
		if (hex < 10) { // rozsah (0-9)
			ext[i] = hex + '0';
		}
		else { // rozsah (A-F)
			ext[i] = hex + 'A' - 10;
		}
	}
} // konec marker::dec2hex

//==================================================//
// Nazev: hex2dec                                   //
// Trida: marker                                    //
// Popis: prevede hexadecimalni znak na cislo	    //
//==================================================//
int marker::hex2dec(char ext) {

	if (ext >= 'A' && ext <= 'F') {
		return ext - 'A' + 10;
	}
	else {
		return ext - '0';
	}
} // konec marker::hex2dec

//===================================================//
// Nazev: utf2symbols                                //
// Trida: marker                                     //
// Popis: prevede utf-8 znaky na ascii reprezentaci  //
//===================================================//
std::string marker::utf2symbols(std::string line) {
	unsigned int count = 0;
	unsigned int index = 0;
	unsigned int neco = line[0];
	unsigned int pocet = 0;

	char pole[2];
	char* buf;
	buf = &pole[0];

	char buffer;
	bool now = true;
	std::string out = "";
	unsigned long int char_value = 0;

	while (neco != '\0') {
		char_value = 0;

		if ((neco >= 0) && (neco < 128)) { // je znak v rozsahu
			buffer = neco;                  // zakladni ASCII sady
			out.append(1, buffer); // pridani do vystupniho retezce
			count++;
			index++;
			neco = line[count]; // nacteni dalsiho znaku
		}
		else {
			out.append("&#x"); // predpona utf-8 znaku
			index += 3;
			pocet = 2;

			for (int i = 6; i > 0; i--) { // na kolik bytu je utf-8 znak
				if (!((neco >> i) & 1)) {
					pocet = 7 - i;
					break;
				}
			}

			for (int i = 0; i < pocet; i++) {

				dec2hex(buf, neco); // prevod na decimalni hodnotu
				for (int j = 0; j < 2; j++) {
					out.append(1, pole[j]); // pridani do vystupniho retezce
					char_value += hex2dec(pole[j]) * pow(16.0, (pocet * 2 - 1) - i * 2 - j);
					index++;
				}

				count++;
				neco = line[count]; // nacteni dalsi casti utf-8 znaku
			}

			out.append(";"); // pripona utf-8 znaku
			index++;
			if (offset_in_bytes)
				insertItem(index, pocet + 4, char_value); // zaindexovani konce utf-8
			else
				insertItem(index, (pocet * 2) + 3, char_value); // zaindexovani konce utf-8
		}                                  // znaku
	}

	ulist.Move = ulist.First; // posunutí indexu

	return out;
} // konec utf2symbols

//=====================================//
// Nazev: next_char                    //
// Trida: marker                       //
// Popis: nacita dalsi znak ze vstupu  //
//=====================================//
void marker::next_char() {
	// na konci radku nacitam dalsi
	if (line.empty() || line_index == line.length()+1) { // konec radku?
		if (ulist.Move != NULL) { // zacatek nacitani?
			index_global += line.length() + 1 - ulist.Move->count_sub;
		} // index konce predchoziho radku
		else {
			index_global = 0; // zacatek
		}
		deleteItems(); // reset seznamu indexu utf-8 znaku
		insertItem(1,0,0); // nastaveni pocatecni hodnoty
		line_index = 0; // reset
		getline(*input, line); // nacteni noveho radku
		line = utf2symbols(line); // prevod utf-8 na ascii
		index_s = 0;
		first_par = true; // prvni slovo?
	}
	// nacitani znakupri
	if (!input->eof()) {
		
		b = a; // predchozi znak
		a = line[line_index]; // akt. znak
		line_index++;
		if (a == ';' && ulist.Move->next != NULL && line_index == ulist.Move->next->offset_id) { // je znak konec utf-8 znaku?
			// existuje dalsi utf-8 znak?
			ulist.Move = ulist.Move->next; // posun v seznamu na dalsi
		}

		if (!to_skip(a) && to_skip(b) && first_par) { //
			index_next = line.find_first_of(delimiters, index_s + 1); // index pro
			first_par = false;                           // backtracking
		}

		if (index_next == std::string::npos) { // neexistuje dalsi slovo?
			index_next = 0; // reset
			first_par = true; // reset
		}
		index++;
	}
} // konec marker::next_char

//==========================================================//
// Nazev: next_word                                         //
// Trida: marker                                            //
// Popis: preskoci na zacatek nasledujiciho vyrazu v texte  //
//==========================================================//
void marker::next_word() {
	index++;
	
	while ((/*iscntrl(a) || isspace(a) ||*/ to_skip(a)) && !input->eof()) {
		//cout << "Index: " << index_s << " ulist.move: " << ulist.Act->offset_id << " value: " << ulist.Act->char_value << endl;
		next_char(); // dalsi znak
		index_s++;
		index--;

		while (ulist.Act->next != NULL && to_skip(ulist.Act->next->char_value) && line_index == ulist.Act->next->offset_id - ulist.Act->next->count_sub) { // je treba se

			ulist.Act = ulist.Act->next;                  // posunout?
			ulist.Move = ulist.Act;                     // index utf-8

			if (ulist.Act->next == NULL) {             // znaku musi byt
				break;                                  // pred line_index
			}

			for (int i = ulist.Act->count_sub; i >= 0; i--) {
				next_char();
				index_s++;
			}

			index--;
		}
	}

	ulist.Move = ulist.Act; // nastaveni indexu na akt. polozku
	if (!begin) { // neni zacatek vyhledavani?
		index_s--;
	}

	begin = false;

	if (ulist.Act->next != NULL) { // existuje dalsi utf-8 znak?
		while (line_index > ulist.Act->next->offset_id) { // je treba se

			ulist.Act = ulist.Act->next;                  // posunout?
			ulist.Move = ulist.Act;                     // index utf-8

			if (ulist.Act->next == NULL) {             // znaku musi byt
				break;                                  // pred line_index
			}
		}
	}

	if (ulist.Act != NULL) { // aktualizovat index zacatku slova?
		first = index_global + index_s + 1 - ulist.Act->count_sub;
	}
} // konec marker::next_word

//======================================//
// Nazev: end_word                      //
// Trida: marker                        //
// Popis: skoci na konec slova v texte  //
//======================================//
void marker::end_word() {
	while (((/*!isspace(a) && !iscntrl(a) &&*/ !to_skip(a)) || a == '\\') && !input->eof()) {
		next_char(); // nacti dalsi znak
	}
	if (((line.length() + 1) == line_index) && first_par) { // jsme na
		was_end = true;                               // konci radku?
	}
} // konec marker::end_word

//======================================================//
// Nazev: save_match                                    //
// Trida: marker                                        //
// Popis: ulozi nalezeny vyraz do promenne typu Tmatch  //
//======================================================//
Tmatch marker::save_match(std::string radek, std::string jmeno, unsigned long int f_index, unsigned long int l_index) {
	Tmatch tmp;
	tmp.first_index = f_index; // index zacatku slova
	tmp.last_index = l_index; // index konce slova
	tmp.line_num = radek; // seznam cisel radku
	tmp.line_name = jmeno; // nazev entity
	return tmp;
} // konec marker::save_match

//=======================================//
// Nazev: lookup                         //
// Trida: marker                         //
// Popis: vyhledavani ve vstupnim texte  //
//=======================================//
std::string marker::lookup() {
	dict_list *my_dict;
	fsa_arc_ptr *dummy = NULL;

	my_dict = &dictionary;
	ANNOT_SEPARATOR = my_dict->item()->annot_sep;
	set_dictionary(my_dict->item());

	potencial_match.first_index = 0;
	potencial_match.last_index = 0;
	index_next = 0;

	set_index(); // reset
	begin = true; // reset
	next_char(); // nacti prvni znak

	while (!input->eof()) { // je konec?
		next_word(); // dalsi slovo
		match_word(dummy->first_node(current_dict));
		// zavola se vyhledavaci funkce
		if (first_par && !was_end) { // prvni slovo?
			index_next = line.find_first_of(delimiters, index_s + 1);
		}
		// inkrementace a reset
		if (was_end)
			next_char();
		line_index = index_next + 1;
		index_s = index_next + 1;
		index = 0;
		first_par = true;
		a = ' ';
		
		if (was_end) {
			was_end = false;
			line_index = 0;
			index_s = 0;
		}
	}
	deleteItems(); // odstran vsechny polozky ze seznamu

	return dataset.results;
} // konec marker::lookup

//=====================================================================
// Nazev: auto_lookup
// Trida: marker
// Popis: vyhledavani ve vstupnim texte
//=====================================================================
std::string marker::auto_lookup(unsigned int many) {
	dict_list *my_dict;
	fsa_arc_ptr *dummy = NULL;

	// premazanie stringu kvoli viacnasobnemu volaniu funkcie v jednej instancii triedy
	dataset.results.clear();
	my_dict = &dictionary;
	ANNOT_SEPARATOR = my_dict->item()->annot_sep;
	set_dictionary(my_dict->item());

	set_index();
	next_char();
	while (!input->eof()) {
		next_word();
		auto_match_word(dummy->first_node(current_dict), many);
	}

	return dataset.results;
} // konec marker::auto_lookup

//=======================================//
// Nazev: lookup_file                    //
// Trida: marker                         //
// Popis: vyhledavani ve vstupnim texte  //
//=======================================//
std::string marker::lookup_file(std::string input_name) {
	set_input_filename(input_name);
	return lookup();
}

//=====================================================================
// Nazev: auto_lookup_file
// Trida: marker
// Popis: vyhledavani v souboru
//=====================================================================
std::string marker::auto_lookup_file(std::string input_name, unsigned int many) {
	set_input_filename(input_name);
	return auto_lookup(many);
} // konec marker::auto_lookup_file

//=======================================//
// Nazev: lookup_string                  //
// Trida: marker                         //
// Popis: vyhledavani ve vstupnim texte  //
//=======================================//
std::string marker::lookup_string(std::string input_string) {
	std::stringstream ss;
	ss.str(input_string);

	set_input(&ss);
	return lookup();
}

//=====================================================================
// Nazev: auto_lookup_string
// Trida: marker
// Popis: vyhladavani ve vstupnim texte
//=====================================================================
std::string marker::auto_lookup_string(std::string input_string, unsigned int many) {
	std::stringstream ss;
	ss.str(input_string);

	set_input(&ss);
	return auto_lookup(many);
} // konec marker::auto_lookup_string

//================================//
// Nazev: set_input_filename      //
// Trida: marker                  //
// Popis: otevre vystupni soubor  //
//================================//
void marker::set_input_filename(std::string name) {
	if (dataset.input_file.is_open()) dataset.input_file.close();
	dataset.input_file.open(name.c_str());

	set_input(&dataset.input_file);
} // konec marker::set_input_filename


//========================================================//
// Nazev: set_input                                      //
// Trida: marker                                          //
// Parameter: input										    //
// Popis: nastavuje vstup									//
//========================================================//
void marker::set_input(std::istream* in) {
	input = in;
}

//========================================================//
// Nazev: match_word                                      //
// Trida: marker                                          //
// Parameter: ukazovatel pro fsa, na zacatek automatu;    //
// Popis: prochazi znak po znaku automatem a hleda shodu  //
//========================================================//
void marker::match_word(fsa_arc_ptr start) {
	bool found; // byla nalezena shoda znaku se zaznamem v automatu?
	bool fin = false; // je konec?
	bool save_mod = false; // je ulozen aspon jeden zaznam?
	fsa_arc_ptr test; // docasne ulozeny uzel
	fsa_arc_ptr next_node; // pracovni uzel
	std::string jmeno; // ukladani nazvu entity
	bool no_number = false; // fragment?

	do { // vyhledavani v KA
		next_node = start.set_next_node(current_dict);
		found = false;
		test = next_node;
		forallnodes(i) { // hledani shody znaku s automatem
			if (a == next_node.get_letter()) {
				jmeno.append(1, a);
				found = true; // znak byl nalezen
				start = next_node; // dalsi uzel
				break;
			}
		} // forallnodes....

		if (!found) {
			end_word();
			if (save_mod) {
				dataset.get_data(potencial_match, FIGA);
				save_mod = false;
			}
			return;
		}

		// ak bol zadany prepinac "-o"
		if (entity_overlapping && to_skip(a) && !to_skip(b))	// vypis kvoli najdenemu oddelovaciemu znaku
		{
			end_word();
			if (save_mod) {  // ak bola rozpoznana entita
				dataset.get_data(potencial_match, FIGA);
				save_mod = false;
			}
		}

		// nacti dalsi znak
		next_char();

		test = start; // ulozeni uzlu
		next_node = start.set_next_node(current_dict);

		forallnodes(i)
		{ // existuje zaznam v automatu
			if (('|' == next_node.get_letter()) && (/*ispunct(a) && (a != '&') || isspace(a) || iscntrl(a) ||*/ to_skip(a) || input->eof()))
			{	// a != '&' je v podmienke kvôli vypisu entity ako casti slova ak za entitou nasleduje utf znak
				if (entity_overlapping || potencial_match.last_index < first || potencial_match.first_index == first)
				{	// zakazanie vratenia prekryvajucej sa entity ak nebol zadany prepinac '-o'
					fin = true; // zacni s hledanim cisel radku
				}
				break;
			}
		}

		if (fin) // byl najity zaznam v automatu, ted se bude rozpoznavat
		{
			if (parse_line_numbers(&no_number, &save_mod, next_node))
				potencial_match = save_match(dataset.radek_line, jmeno, first, (first + index - 2) - (ulist.Move->count_sub - ulist.Act->count_sub));	// ulozeni info o entite do temp struktury
			else
				potencial_match = save_match(dataset.radek_line, jmeno, first, (first + index - 2));
			fin = false;
		}
			
		if (fin && input->eof())	// vypis entity (ak sa nejaka nasla) kvoli koncu riadku
			dataset.get_data(potencial_match, FIGA); // uloz zaznam do stringu

		next_node = test; // navraceni se na puvodni uzel
		start = test;

	} while (!input->eof() && found);

} // konec marker::match_word

//=====================================================================
// Nazev: auto_match_word
// Trida: marker
// Parametry: 1) ukazatel pro fsa, na zacatek automatu;
//            2) kolik entit ma byt vypsano na vystup
// Popis: prochazi znak po znaku automatem a hleda shodu, pote dohledava
//        vsechny mozno pokracovani (max = kolik)
//=====================================================================
void marker::auto_match_word(fsa_arc_ptr start, int kolik) {

	bool found = false; // byla nalezena shoda znaku se zaznamem v automatu?
	bool fin = false; // ma se zacit hledat potencialni zaznamy?
	bool done = false; // je dokonceno hledani potencialnich zaznamu?
	bool con_search = false; // maji se hledat cisla radku?
	bool go_back = false; // nasel se stejny zaznam s lehce jinym nazvem?

	std::list <string> zasobnik_jmeno;
	std::list <fsa_arc_ptr> zasobnik_pole;

	std::string jmeno[kolik]; // ukladani nazvu entity
	for (int i = 0; i < kolik; i++) {
		jmeno[i].clear();
	}
	std::string preradek[kolik];
	for (int i = 0; i < kolik; i++) {
		preradek[i].clear();
	}

	fsa_arc_ptr test; // docasne ulozeny uzel
	fsa_arc_ptr pole[kolik]; // uzel pro jednotlive zaznamy
	fsa_arc_ptr next_node; // pracovni uzel

	int many;

	do { // vyhledavani v KA
		next_node = start.set_next_node(current_dict);
		found = false;

		test = next_node;

		forallnodes(i) { // hledani shody znaku s automatem
			if (a == next_node.get_letter()) { // shoda?

				start = next_node; // ulozeni uzlu s nalezenou shodou

				for (int i = 0; i < kolik; i++) {
					pole[i] = next_node; // ulozeni uzlu do vsech polozek
				}

				jmeno[0].append(1, a);
				found = true; // znak byl nalezen
				break;
			}
		} // forallnodes....

		if (!found && a == '\0' && (line.length() > 0)) { // nebyla nalezena shoda?
			end_word();
			fin = true; // zacni dohledavat
		}
		else if (!found) {
			end_word();
			return;
		}

		next_char();

		if (fin) { // bylo ukonceno vyhledavani v KA, zacina dohledavani potec. zaznamu

			for (int x = 1; x < kolik; x++) { // ulozeni spolecneho zakladu
				jmeno[x] = jmeno[0];
			}

			fsa_arc_ptr temp;
			fsa_arc_ptr prvni;

			bool shot = false; // nasel shodu?
			bool end = false; // konec uzlu?
			bool first = true; // prvni uzel, ktery je testovan
			bool orig_node = false; // byl nalezen vysledek uz ve vyhledavani?
			bool no_number = false; // fragment?

			int act_cnt = 0; // aktualni vrchol (promenny)
			size_t indexik = 0; // index do abedy
			int cnt = 0; // aktualni vrchol (stabilni)
			std::string temp_name; // docasne ulozeni nazvu
			std::string abeda = " ',-.aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ&0123456789#;|"; // abeceda priority (pismena vice vlevo maji vyssi prioritu)
			size_t max = abeda.length(); // max znaku v abede
			next_node = pole[cnt].set_next_node(current_dict); // nastaveni na zacatek uzlu po vyhledavanem reztezci

			if (next_node.get_letter() == '|') { // pokud byl zaznam nalezen uz pri vyhledavani
				prvni = next_node; // ulozeni puvodniho uzlu
				orig_node = true; // byl nalezen ve vyhledavani
			}

			while (cnt < kolik) { // pobezi dokud budou polozky k dispozici
				do {
					temp = next_node; // ulozeni puvodniho uzlu
					do {
						many = 0;
						forallnodes(i) { // pro kazdy nasledujici uzel v akt. uzlu
							many++;
							if (abeda[indexik] == next_node.get_letter() || 0 == next_node.get_letter() || indexik >= max) { // je znak v uzlu?

								if (next_node.get_letter() == '|' || 0 == next_node.get_letter()) { // je znak konec uzlu?
									if (many > 1) {
										int pocet = 0;
										zasobnik_pole.push_front(pole[kolik - 1]);
										zasobnik_jmeno.push_front(jmeno[kolik - 1]);

										for (int x = cnt; x < (kolik - 1); x++) { // posunuti polozek dozadu ... aby se nestratil dosavadni posun
											pole[kolik - pocet - 1] = pole[kolik - pocet - 2];
											jmeno[kolik - pocet - 1] = jmeno[kolik - pocet - 2];
											pocet++;
										} // for
									} // if (many....

									shot = true; // nasel shodu
									end = true; // konec dohledavani
									break; // hura na vypis zaznamu do stringu
								} // if (next....

								if (indexik >= max) {		// nasleduje neznamy znak (nie je v abecede)
									shot = true;
									end = true; // konec dohledavani
									break;
								}

								temp_name = jmeno[cnt];

								jmeno[cnt].append(1, abeda[indexik]);	// ulozeni znaku do jmena (pokud to neni | )
								shot = true; // nasel shodu
								indexik = 0; // restart indexovani

								test = next_node; // ulozeni uzlu s nalezenym znakem
								first = true; //

								act_cnt = cnt; // nastaveni aktualniho vrcholu

								do { // smycka, ktera upresnuje zaznamy
									forallnodes(i) { // prochazeni vsech uzlu (sem se dostane pouze tehdy, pokud neni | )

										if (abeda[indexik] == next_node.get_letter()) { // nasel shodu v abede

											if (abeda[indexik] == '|') { // je znak konec uzlu?
												int pocet = 0;
												zasobnik_pole.push_front(pole[kolik - 1]);
												zasobnik_jmeno.push_front(jmeno[kolik - 1]);

												for (int x = cnt; x < (kolik - 1); x++) { // posunuti polozek dozadu ... aby se nestratil dosavadni posun
													pole[kolik - pocet - 1] = pole[kolik - pocet - 2];
													jmeno[kolik - pocet - 1] = jmeno[kolik - pocet - 2];
													pocet++;
												} // for...

												jmeno[cnt] = temp_name;
												shot = true; // nasel shodu
												end = true; // ?
												con_search = true;
												break; // hura na vypis
											} // if (abeda...

											if (act_cnt >= kolik) { // ma cenu pokracovat ... kolik je max a yy muze byt vzdy jen o 1 mensi
												break;
											} // if (act....

											if (act_cnt > cnt) {
												int pocet = 0;
												zasobnik_pole.push_front(pole[kolik - 1]);
												zasobnik_jmeno.push_front(jmeno[kolik - 1]);

												for (int x = act_cnt; x < (kolik - 1); x++) { // pokud yy neni aktualni vrchol, pak se budou rotovat data
													pole[kolik - pocet - 1] = pole[kolik - pocet - 2]; // rotuje nody dozadu ... napr. 1 -> 2
													jmeno[kolik - pocet - 1] = jmeno[kolik - pocet - 2]; // rotuje nazev
													pocet++;
												}
											} // if (act....

											pole[act_cnt] = next_node; // ulozi node do aktualniho vrcholu

											if (act_cnt > cnt) {
												jmeno[act_cnt] = temp_name;
												jmeno[act_cnt].append(1, abeda[indexik]);
											} // if (act....

											act_cnt++; // zvysi aktualni vrchol
											break;
										}
									}

									indexik++; // posune se na dalsi znak v abecede priority
									next_node = temp; // vrati se na puvodni node, od ktereho se zacina

									if (max <= indexik) { // pokud se dosahlo max. indexu v abecede priority
										break; // konec
									}

									if (act_cnt == kolik) {
										indexik = max - 1; // nastaveni indexu na znak '|'
									}

								} while ((act_cnt < (kolik + 1)) && (indexik < max)); // dokud bude vrchol v rozmezi a index do abecedy v rozmezi

								next_node = test.set_next_node(current_dict); // nastavi se dalsi pokrok v prvnim uzlu
								test = next_node; // ulozi se aktualni zacatek uzlu

								break;
							} // if (abeda....
						} // forallnodes....

						indexik++; // zvysi se index v abecede priority

						if (!first) { // prvni testovaci uzel?
							next_node = pole[cnt].set_next_node(current_dict);
							// nacte nasleduji uzel do akt. uzlu
						}
						else {
							next_node = test; // vrati zacatek uzlu do akt. uzlu
						}

					} while (!shot); // nasel shodu?

					shot = false; // reset
					indexik = 0; // reset
					next_node = test; // vrati zacatek uzlu do aktualniho uzlu

				} while (!end); // byl identifikovan konec uzlu?

				end = false; // reset
				first = false; // reset

				if (con_search) { // dohledavani skoncilo na zacatku uzlu
					next_node = pole[cnt];
				}

				if (orig_node) { // puvodni uzel, ktery byl nalezen uz ve vyhledavani
					next_node = prvni; // nacteni puvodniho uzlu do akt. uzlu
					orig_node = false; // reset
				}

				con_search = false; // reset
				if (next_node.get_letter() == '|') { // je zde info o cislo radku?
					con_search = true;
				}
				else {
					forallnodes(i)
						if (next_node.get_letter() == '|') { // je zde info o cislo radku
							con_search = true;
							break;
						}
				} // if (next....

				fin = false; // reset
				no_number = false; // reset

				if (con_search) { // nacitanie cisel riadkov z automatu
					if (parse_line_numbers(&no_number, NULL, next_node)) {
						potencial_match = save_match(dataset.radek_line, jmeno[cnt]);
						preradek[cnt] = dataset.radek_line;
					}
					else
						potencial_match = save_match(dataset.radek_line, jmeno[cnt]); // uloz info do zaznamu
					fin = true;
				} // if (con_s....
				con_search = false; // reset

				for (int i = 0; i < cnt; i++) {	// kontrola pred ulozenim
					if (dataset.radek_line == preradek[i]) {
						fin = false; // neulozi se do stringu
						go_back = true;
					}
					if (!strcmp(jmeno[i].c_str(), jmeno[cnt].c_str()))
						return;  // zda se neopakuji zaznamy pokud ano, tak skonci
				}

				if (no_number) {	// vyhodenie fragmentu z pola
						fin = false;  // neulozi sa do vypisoveho stringu
						go_back = true;	 // posunut entity
				}

				if (fin == true) {
					dataset.get_data(potencial_match, AUTOCOMPLETE); // nahrani zaznamu do vysledneho stringu
					fin = false; // reset
				}

				if (!go_back) { 
					cnt++; // zvyseni aktualniho vrcholu polozek
				}
				else { // Posunuti entit (dalsi zaznam bude na stejnem miste)
					for (int x = cnt; x < (kolik - 1); x++) {
						pole[x] = pole[x + 1];
						jmeno[x] = jmeno[x + 1];
					}

					size_t x = 0;
					while (x < max) {
						if (abeda[x] == zasobnik_pole.front().get_letter())
							break;
						x++;
					}

					x++;
					while (x < max) {
						next_node = zasobnik_pole.front();
						forallnodes(i) {
							if (abeda[x] == next_node.get_letter()) {
								x = max;
								break;
							}
						}
						x++;
					}

					pole[kolik - 1] = zasobnik_pole.front();
					zasobnik_pole.pop_front();
					jmeno[kolik - 1] = zasobnik_jmeno.front();
					zasobnik_jmeno.pop_front();
					//jmeno[kolik-1].append(1, next_node.get_letter());
				}
				go_back = false;
				next_node = pole[cnt].set_next_node(current_dict); // nacteni uzlu dalsi polozky do akt. uzlu
			} // while (cnt....

			done = true; // dohledavani skoncilo
		} // if (fin....

	} while ((!input->eof() && found));

} // konec marker::auto_match_word

//=====================================================================
// Nazev: parse_line_numbers
// Trida: marker
// Parametry: 1) najdeny fragment
//            2) je ulozeny aspon jeden zaznam
//            3) uzol obsahujuci '|'
// Popis: prochazi znak po znaku automatem a hleda shodu, pote dohledava
//        vsechny mozno pokracovani (max = kolik)
//=====================================================================
bool marker::parse_line_numbers(bool *no_number, bool *save_mod, fsa_arc_ptr next_node) {

	dataset.radek_line.clear();                  // vsech radku v KB
	fsa_arc_ptr start = next_node;
	next_node = start.set_next_node(current_dict);
	start = next_node;
	unsigned int radek = 0; // temp pro hodnotu radku
	char znak = next_node.get_letter(); // prvni znak za '|'
	*no_number = false; // reset
	bool run = true; // ma zacit hledat?
	int bitshift = 21;      // bitovy posuv kvoli kodovaniu cisla v KA
	int byte_number = 4;    // cislo riadku je v KA zapisane na 4 bajtoch

	if (znak == '\n') { // chybny format?
		dataset.save_line(radek, true);
		if (save_mod)	// toto je kontrola spravnosti pointra
			*save_mod = true;
		run = false;
		return false;
	}

	while (run) { // ma se zacit vyhledavat?
		if ((byte_number > 0) && (znak != 'N')) {
			radek |= (((int)(znak & 0xFE)) << bitshift) >> 1;
			// odfiltrovanie spodneho bitu a priradenie bajtu do cisla
			bitshift -= 7;
			byte_number--;  // posun na dalsi byte
		}

		if (znak == 'N')    // ('N' = 78 in ASCII)
			*no_number = true;	// fragment entity?

		fsa_arc_ptr next_node = start.set_next_node(current_dict);
		start = next_node;
		znak = next_node.get_letter(); // dalsi znak v uzlu

		if ((byte_number == 0) && (znak % 2 == 0x01)) { // ak prave nacitany znak patri uz do dalsieho cisla
			dataset.save_line(radek, false);
			radek = 0;
			byte_number = 4;
			bitshift = 21;
		}

		if ((znak == '\0') || (znak == '\n')) { // posledne cislo v riadku?      ('\0' = 0 ... '\n' = 10 in ASCII)
			if ((radek != 0) || (no_number)) {
				dataset.save_line(radek, true);
				if (save_mod)	// toto je kontrola spravnosti pointra
					*save_mod = true;
			}
			return true;
		}
	}
}