# nutnost provest make souboru kb_loader_fast.cc (nutno odkomentovat
# v souboru Makefile)

import commands
import os
from ctypes import *

#@profile
def my_func():
	count = commands.getoutput('wc -l ../make_automat/KB.all | cut -d" " -f1')
	max = int(count)

	lib = cdll.LoadLibrary('./kb_loader.so')
	lib.queryTree.restype = POINTER(c_char_p)
	lib.queryTree.argtypes = [c_char_p,c_int]

	s = lib.queryTree("../make_automat/KB.all",max);
	return s

def my_print(value):
	count = commands.getoutput('wc -l ../make_automat/KB.all | cut -d" " -f1')
	max = int(count)
	for x in range(0,max):
		print value[x] # pristup pouze k celemu radku entity

if __name__ == '__main__':
	value = my_func()
	my_print(value)
