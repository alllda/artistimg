/*
Copyright 2014 Brno University of Technology

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


//==========================================================//
// Soubor: figa.h v0.4                                      //
// Autor: Marek Visnovsky, xvisno00@stud.fit.vutbr.cz       //
// Upravil: Karel Brezina, xbrezi13@stud.fit.vutbr.cz       //
// Popis: knihovna pro znaceni geografickych nazvu v texte  //
//==========================================================//

#ifndef FIGA_H
#define FIGA_H

#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <sstream>

#include "fsa.h"
#include "common.h"
#include "nstr.h" 

// struktura pro nalezene vyrazy
typedef struct {
    unsigned long int first_index, last_index; // pozice entity 
	std::string line_num;                   // radek/ky v KB 
	std::string line_name;						// nazev entity 
} Tmatch; 

typedef struct ListItem TListptr; 
// struktura seznamu pro indexy utf-8 znaku
typedef struct { 
	TListptr* First; // Prvni utf-8 znak
	TListptr* Act; // Aktivni -,,-
	TListptr* Move; // Dalsi -,,-
} ListOfIndex; 
// struktura indexu utf-8 znaku
struct ListItem {
	int count_sub; // kolik bytu zabira
	int offset_id; // na kterem indexu se nachazi
	unsigned long int char_value;
	TListptr* next; // dalsi utf-8 znak
};

//===========================//
// Nazev: dataset            //
// Popis: prace se zaznamem  //
//===========================//
class data_proc {
protected:    	
public:
	std::string results; // ulozeni vysledku
	std::string radek_line; // uklada informace o radcich entity
	std::ifstream input_file; // soubor, ze ktereho se ctou data

	void get_data(Tmatch match); // ulozi vsechny informace do stringu
	void save_line(unsigned long int radek, bool end); // ulozi cislo
																		// radku
	data_proc() {results = ""; radek_line = "";}; // konstruktor
};

//===================================================//
// Nazov: marker                                     //
// Popis: znaci shody mezi vyrazy z fsa a ze vstupu  //
//===================================================//
class marker : public fsa {
protected:
	char a; // akt. znak
	char b; // predchozi znak
	unsigned long int index; // akt. index
	unsigned long int line_index; // index pro nacitani znaku
	unsigned long int index_s;
	unsigned long int index_next;
	unsigned long int index_global; // index konce predchoziho radku

	bool first_par; // prvni slovo?
	bool was_end; // byl na konci?
	bool begin; // zacatek?
	short jednou; // jen jednou bude true (1) :-) 

	ListOfIndex ulist; // seznam indexu utf-8 znaku

	std::string line; // radek na vstupe 

	unsigned long int first; // pozice prveho znaku najdeneho 
									 // vyrazu 
	std::istream* input; // vstup (soubor/string/stdin)

	Tmatch potencial_match; // pomocna promenna pri 
									// viceslovnych vyrazech     

	void set_index(); // vynuluje index 

	void next_char(); // nacita nasledujici znak v aktualnim vstupu (soubor/string/stdin)

	void next_word(); // preskoci na dalsi vyraz v aktualnim vstupu (soubor/string/stdin)

	void end_word();  // skoci na konec slova v aktualnim vstupu (soubor/string/stdin)

	Tmatch save_match(unsigned long int f_index, unsigned long int l_index, std::string radek_line, std::string jmeno); // ulozi najdeny vyraz
	void match_word(fsa_arc_ptr start); // pruchod automatem 
    
	bool is_first_word; // prvni slovo?

public:
	data_proc dataset; // prace se zaznamem 
	marker(word_list *dict_names, const char *language_file = NULL) : fsa(dict_names, language_file) {}; // konstruktor
	std::string lookup(); // vyhladavani ze vstupu (soubor/string/stdin)
	std::string lookup_file(); // vyhladavani ze vstupu (soubor/string/stdin)
	
	// dve dalsi lookup metody kvuli kompatibilite s ostatnimi programy
	std::string lookup_file(std::string input_name); // vyhledavani v souboru
	std::string lookup_string(std::string input_string); // vyhledavani ve stringu
	
	~marker(void) { 
		delete [] current_dict; // destruktor
	}
	bool set_input_filename(std::string name); // nacteni vstupniho souboru
	void set_input(std::istream*);
	bool to_skip(unsigned long int znak); // preskocit?
	void dec2hex(char*, unsigned int); // prevede na decimalni hodnotu
	int hex2dec(char); // prevede na decimalni hodnotu
	std::string utf2symbols(std::string); // prevede utf-8 na ascii

	void initList(); // pocatecni inicialzace
	void insertItem(unsigned int offset, unsigned int value, unsigned long int char_value); // vlozi 
																				 // prvek
	void deleteItems(); // smaze vsechny polozky v seznamu
};

#endif
