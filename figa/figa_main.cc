/*
Copyright 2014 Brno University of Technology

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

//=====================================================//
// Soubor: figa_main.cc v0.8                           //
// Autor: Marek Visnovsky, xvisno00@stud.fit.vutbr.cz  //
// Upravil: Karel Brezina, xbrezi13@stud.fit.vutbr.cz  //
// Upravil: Peter Hostacny, xhosta03@stud.fit.vutbr.cz //
// Popis: vyhledavani entit ve fulltextu               //
//=====================================================//

#include <iostream>
#include <fstream>
#include <unistd.h>

#include "fsa.h"
#include "nstr.h"
#include "common.h"
#include "figa.h"

//====================================//
// Nazev: print_help                  //
// Popis: vytiskne napovedu na stdin  //
//====================================//
void print_help () {
	std::cout << "updated: FItGAzetter v0.7c, November 16 th, 2013, Karel ";
	std::cout << "Brezina, xbrezi13@stud.fit.vutbr.cz" << std::endl;
	std::cout << "FItGAzetteer v0.35c, September 14th, 2010, Marek ";
	std::cout << "Visnovsky, xvisno00@stud.fit.vutbr.cz" << std::endl;
	std::cout << "based on: fsa Ver. 0.49, March 18th, 2009, (c) Jan Daciuk";
	std::cout << ",jandac@eti.pg.gda.pl" << std::endl;
	std::cout << "usage:" << std::endl << "figav07c [options]..." << std::endl;
	std::cout << "options" << std::endl;
	std::cout << "-d dictionary_file -> automaton file" << std::endl;
	std::cout << "-f file -> file with words" << std::endl;
	std::cout << "-p -> for print out string" << std::endl;
	std::cout << "-h -> this help" << std::endl;
	return;
} // konec print_help

//=========================//
// Nazev: main             //
// Popis: spousti program  //
//=========================//
int main (const int argc, char *argv[]) {

	word_list dict;
	const char *lang_file = NULL;
	char *fsa_name = NULL; // nazev konecneho automatu
	char *file_name = NULL; // nazev souboru, ze ktereho se ctou data
	std::string name_file;
	int c, error = 1;
	bool file = false; // cteni dat ze souboru?
	bool print_bool = false; // tisk na stdin?

    // zpracovani argumentu
	while ((c = getopt(argc, argv, "d:f:ph")) != -1)
		switch (c) {
			case 'd':
				fsa_name = optarg;
				error--;
				break;
			case 'f':
				file = true;
				file_name = optarg;
				name_file = file_name;
				break;
			case 'p':
				print_bool = true;
				break;
			case 'h':
				error = 2;
				break;
			default:
				std::cerr << "FIGA ERROR: Unrecognized option" << std::endl;
				error = 2;
			}

        // osetreni chybejicich argumentu
	if (error > 0) {
		if (error != 2)
			std::cout << "FIGA ERROR: One dictionary file must be specified!" << std::endl;
		else
			print_help(); // tisk help
		return 1;
	}

        // overenie existencie automatu
	std::ifstream soubor(fsa_name);
	if (!soubor.is_open()) { // chyba pri otvarani automatu
		std::cerr << "FIGA ERROR: Automata file couldn't open!" << std::endl;
		return 2;
	}
	soubor.close();
	 
	dict.insert(fsa_name); // nacteni slovniku (automatu)
	marker seek_names(&dict, lang_file); // inicializace slovniku

	seek_names.set_input(&std::cin);

        // overenie existencie vstupneho suboru
    if (file) {  // bol zadany prepinac -f ?
		seek_names.set_input_filename(file_name);
    }

	if (print_bool) { // tisk na cout?
		std::cout << seek_names.lookup(); // ze stdin
	}
	else { // bez tisku na cout
		seek_names.lookup();
	}

	return 0;
} // konec main
