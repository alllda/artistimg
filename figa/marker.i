%module marker
%{
#include "figa.h"
#include "fsa.h"
#include "common.h"
#include "nstr.h"
%}

%include "std_string.i"

template <class T>
class list {
public:
  list(void);
  void insert(const T *new_item);
};

%template(myList) list<char>;

typedef list<char> word_list;
  
class marker : public fsa {   
public:      
    marker(word_list *dict_names, const char *language_file = NULL) : fsa(dict_names, language_file) {};
    std::string lookup();              
	std::string lookup_file(std::string input_name);
	std::string lookup_string(std::string input_string);
};
