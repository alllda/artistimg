import os
import sys
import operator

fileIn = open(sys.argv[1], 'r')
fileOut = open(sys.argv[2], 'w')

stats = {}

for line in fileIn:
    try:
	autor = line.split('\t')[1]
    except:
	continue
    if autor in stats:
	stats[autor] += 1 
    else:
	stats[autor] = 1

#for keys in stats.keys():
#    print keys + " : " + str(stats[keys])

sorted = sorted(stats.iteritems(), key=operator.itemgetter(1))

for item in sorted[::-1]:
    #print item[0] + " : " + str(item[1])
    fileOut.write(item[0] + " : " + str(item[1]) + "\n")
#print sorted
