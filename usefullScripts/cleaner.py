import sys


input = open(sys.argv[1],'r')
output = open(sys.argv[2],'w')

for line in input:
    splited = line.split('\t')
    if(len(splited) == 3):
	if(len(splited[2]) < 127 and len(splited[2]) > 10):
	    if(not splited[2].startswith('//')):
		output.write(line)
