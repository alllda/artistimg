import gzip
import sys
import re

def getArtistDict(file):
	file = open(file,'r')

	artist = {}
	try:
		for line in file:
			artist[line.split("\t")[0]] = line.split("\t")[1].replace('\n','')
		file.close()
	except:
		print ('Artist file is not in correct format')
		sys.exit()
	return artist


artists = getArtistDict("artists3.txt")

file = gzip.open(sys.argv[1],'r').read()[:50000000]
print len(file)

counter = 0
for key in artists.keys():
	print counter
	counter += 1
	for m in re.finditer(artists[key], file):
         print(artists[key], m.start(), m.end())
