import os
import sys

counter = 0
size = 0
for dirname, dirnames, filenames in os.walk(sys.argv[1]):
    for filename in filenames:
	filePath = os.path.join(dirname, filename)
	#only use .warc files
	if filePath.endswith('.warc.gz'):
	    counter+=1
	    size += os.stat(filePath).st_size

print "FileCount: " + str(counter)
print "Size: " + str(size) + "B, "+str(size/1000000000000)+"TB"
