
# Configuration stuff
# 1. parameter - original file
# 2. parameter - numbers of file
# 3. parameter - output files
fspec=$1
num_files=$2

# Work out lines per file.

total_lines=$(cat ${fspec} | wc -l)
((lines_per_file = (total_lines + num_files - 1) / num_files))

# Split the actual file, maintaining lines.

split --lines=${lines_per_file} ${fspec} $3.

# Debug information

echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"    
wc -l $3.*
