#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
-----------------------------------------------------------------------------------------
Projekt: Img clueweb12 artists

Autor:   Aleš Raszka
         xraszk03@stud.fit.vutbr.cz

Vedoucí: Smrz Pavel, doc. RNDr., Ph.D.
         smrz@fit.vutbr.cz

Wiki:    https://knot.fit.vutbr.cz/wiki/index.php/Img_clueweb12_artists
Last update: 8.8.2014
----------------------------------------------------------------------------------------
'''
import gzip
import io
import sys
import os
import re
import figa2.sources.marker as figa
#from BeautifulSoup import BeautifulSoup
import urlparse
from multiprocessing import Process, Manager
import Queue
import threading

def printHelp():
    print('-----------------------------------------------------------------------------------')
    print('Script search for artist in .warc files and save info about all images on HTML page')
    print('Output format:')
    print('    row in KBdata/KBartists    name    image URL')
    print('Usage:')
    print('    python imageList.py --help or -h')
    print('    python imageList.py arg1 output')
    print('        arg1   - .warc file or path where look for .warc file')
    print('        output - output file ')
    print('-----------------------------------------------------------------------------------')

def is_absolute(url):
	return bool(urlparse.urlparse(url).netloc)


def getSubstring(start, end, text):
    return text[start-1:end].encode("utf-8")

class PrintThread(threading.Thread):
    def __init__(self, queue, output):
        super(PrintThread, self).__init__()
        self.output = output
        self.queue = queue

    def run(self):
        while True:
            result = self.queue.get()
            #print result
            self.output.write(result)
            self.queue.task_done()

class ImageFinderThread(threading.Thread):
    def __init__(self, queue, printQueue,dictionary):
	super(ImageFinderThread, self).__init__()
	self.queue = queue
	self.printQueue = printQueue
	self.dictionary = dictionary
	

    def run(self):
	while True:
	    filePath = self.queue.get()
	    print "Searching in: " + filePath 
	    correct = 0
	    incorrect = 0
	    inputText = gzip.open(filePath,'r').read()
	    pages = inputText.split("WARC/1.0")[2:]
	    for page in pages:
		seekNames = figa.marker(self.dictionary, None, True, False)
		figaOutput = seekNames.lookup_string(page) # figa searching

		#print figaOutput
		for line in figaOutput.splitlines():
			lineParts = line.split('\t')
			# get 600 char in surrounding area
			if lineParts[3] == page[int(lineParts[1])-1:int(lineParts[2])]:
			    input_len = len(page)
			    correct += 1
			    start =int(lineParts[1])-300
			    if  start < 0:
				start = 0
			    end = int(lineParts[1])+300
			    if end > input_len:
				end = input_len
			    text = inputText[start:end]
			    images = re.findall("<img [^>]*src=\"([^\"]+)",text) # search for src in img HTML tag
			    if not images:
				continue # no images found
			    for img in images:
				if is_absolute(img): # only images with absolute path
				    
                                    outputItem = u'{}\t{}\t{}\n'.format(lineParts[0],lineParts[3].decode('utf-8'),img)
                                    #print outputItem
                                    self.printQueue.put(outputItem)
				    #output.write(u'{}\t{}\t{}\n'.format(lineParts[0],lineParts[3].decode('utf-8'),img))
			else:
			    incorrect += 1 
			    #print line + " = '" + inputText[int(lineParts[1])-1:int(lineParts[2])] +"'"
			#print line + " - \"" + getSubstring(int(lineParts[1]),int(lineParts[2]), unicodeInput) + "\""
		
			
		    
	    print correct, incorrect
	    #output.close()
	    self.queue.task_done()


def findImages(inputFiles, output):
	processList = []
	counter = 0
	counter2 = 0
	processCount = 4 #count of process
	#prepare figa tools
	dictionary = figa.myList()
	dictionary.insert("automata.fsa")

	print "Files: " + str(len(inputFiles))
	
	queue = Queue.Queue()
        printQueue = Queue.Queue()
	
        t = PrintThread(printQueue, output)
        t.daemon = True
        t.start()
        
        for file in inputFiles: 
	    queue.put(file)
	
	for _ in range(processCount):
	    t = ImageFinderThread(queue, printQueue, dictionary)
	    t.daemon = True
	    t.start()
        

	
	queue.join()
        print "Job DOne"
        printQueue.join()
	#output.close()

def main():
	if(len(sys.argv) == 2 and (sys.argv[1] == '--help' or sys.argv[1] == '-h')):
		printHelp()
		sys.exit()
	elif(len(sys.argv) == 3):
		inputFiles = []
		#open output file
		try:
		    fileOut = io.open(sys.argv[2],'a',encoding='utf-8')
		except:
		    print('Can\'t open output file')
		    sys.exit()
		if(not os.path.exists(sys.argv[1])):
		    print('Path not exists')
		    sys.exit()
		if(os.path.isfile(sys.argv[1])): # input is file
		    inputFiles.append(sys.argv[1]) #add to list
		    findImages(inputFiles,fileOut)
		elif(os.path.isdir(sys.argv[1])): #input is path
		    #browse through directories 
			for dirname, dirnames, filenames in os.walk(sys.argv[1]):
				for filename in filenames:
					filePath = os.path.join(dirname, filename)
					 #only use .warc files
					if filePath.endswith('.warc.gz'):
						inputFiles.append(filePath)
			findImages(inputFiles,fileOut)
		else:
		    print ('Bad arguments')
		    printHelp()
		fileOut.close()
		sys.exit()
	else:
		print ('Bad arguments')
		printHelp()
		sys.exit()

if __name__ == "__main__":
        main()
			
