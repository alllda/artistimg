#!/usr/bin/env python3


import sys
import codecs
import csv

in_file = sys.argv[1]
out_file = sys.argv[2]

with open(out_file, 'w') as out:
	with open(in_file) as f:
		reader = csv.reader(f, delimiter='	', quoting=csv.QUOTE_NONE)
		i = 0
		for row in reader:
			if row[0][0] == "a":
				for i in range(25):
					out.write(row[i] + chr(9))
				out.write(row[25] + "\n")

